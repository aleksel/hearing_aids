<?php

namespace app\components\helpers;

use app\models\Pages;
use yii\web\UrlManager;

/**
 * Class StaticPagesHelper
 * @package app\components\helpers
 */
class StaticPagesHelper
{
    private static $instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function getInstance()
    {
        if (self::$instance instanceof self) {
            return self::$instance;
        }

        return self::$instance = new self();
    }

    public function run()
    {
        $urlManager = \Yii::$app->urlManager;

        $requestUrl = trim($_SERVER['REQUEST_URI'], '/');

        $page = Pages::find()->where(['url' => $requestUrl, 'is_showing' => "1"])->one();
            //$urlManager->addRules(['admin' => '/admin/user/index']);
        if ($page) {
            \Yii::$app->params['static_page'] = $page;
            //$urlManager->addRules(['<controller:\w+>/<action:\w+>' => '<controller>/<action>',]);
            $urlManager->addRules([$requestUrl => 'pages']);
        }
    }
}
