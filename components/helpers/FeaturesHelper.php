<?php

namespace app\components\helpers;

/**
 * Class FeaturesHelper
 * @package app\components\helpers
 */
class FeaturesHelper
{
    /**
     * @param array $features
     * @param array $numbers
     */
    public static function getFeatures($features, array $numbers)
    {
        $result = '';

        foreach ($numbers as $number) {
            if (@$features[$number]['value']) {
                $result .= '<div>';
                $result .= $features[$number]['name'];
                $result .= ':<b> ';
                $result .= $features[$number]['value'];
                $result .= '</b>';
                $result .= "</div>\r";
            }
        }

        echo $result;
    }
}
