<?php
/**
 * Todo Добавить коммент
 */

namespace app\components\widgets;

class Nav extends \yii\bootstrap\Nav
{
    /**
     * Checks whether a menu item is active.
     * This is done by checking if [[route]] and [[params]] match that specified in the `url` option of the menu item.
     * When the `url` option of a menu item is specified in terms of an array, its first element is treated
     * as the route for the item and the rest of the elements are the associated parameters.
     * Only when its route and parameters match [[route]] and [[params]], respectively, will a menu item
     * be considered active.
     * @param array $item the menu item to be checked
     * @return boolean whether the menu item is active
     */
    protected function isItemActive($item)
    {
        $requestUrl = trim($_SERVER['REQUEST_URI'], '/');
        $requestUrl = '/' . $requestUrl;

        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = $item['url'][0];
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }

            if ($route === $requestUrl) {
                return true;
            }
        }

        return false;
    }
}
