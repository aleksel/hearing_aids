<?php

namespace app\components\exceptions;

use yii\base\Exception;

/**
 * Class FileExistsException
 * @package app\components\exceptions
 */
class FileExistsException extends Exception
{
    public $attribute = '';

    public function __construct($message = "", $code = 0, \Exception $previous = null, $attribute = null)
    {
        $this->attribute = $attribute;
        \Exception::__construct($message, $code, $previous);
    }
}
