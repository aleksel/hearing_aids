<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/**
 * Top menu definition.
 *
 * @var $this \yii\web\View
 */
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl'   => Yii::$app->homeUrl,
    'options'    => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items'   => [
        [
            'label' => 'Пользователи',
            'url'   => ['/admin/user/index'],
            'items' => [
                ['label' => 'Список пользователей', 'url' => ['/admin/user/index']],
                ['label' => 'Создать пользователя', 'url' => ['/admin/user/create']]
            ]
        ],
        [
            'label' => 'Меню',
            'url'   => ['/admin/menu'],
            'items' => [
                ['label' => 'Меню', 'url' => ['/admin/menu/index']],
                ['label' => 'Изменить меню', 'url' => ['/admin/menu/update']]
            ]
        ],
        [
            'label' => 'Страницы',
            'url'   => ['/admin/pages'],
            'items' => [
                ['label' => 'Список страниц', 'url' => ['/admin/pages/index']],
                ['label' => 'Создать страницу', 'url' => ['/admin/pages/create']]
            ]
        ],
        [
            'label' => 'Баннеры',
            'url'   => ['/admin/banners'],
            'items' => [
                ['label' => 'Список баннеров', 'url' => ['/admin/banners/index']],
                ['label' => 'Создать баннер', 'url' => ['/admin/banners/create']]
            ]
        ],
        [
            'label' => 'Товары',
            'url'   => ['/admin/goods'],
            'items' => [
                ['label' => 'Список товаров', 'url' => ['/admin/goods/index']],
                ['label' => 'Создать товар', 'url' => ['/admin/goods/create']],
                ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                ['label' => 'Список типов характеристик', 'url' => ['/admin/feature-types/index']],
                ['label' => 'Создать тип характеристики', 'url' => ['/admin/feature-types/create']],
                ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                ['label' => 'Список всех характеристик', 'url' => ['/admin/features']],
                ['label' => '', 'url' => ['#'], 'options' => ['class' => 'divider']],
                ['label' => 'Список типов товаров', 'url' => ['/admin/good-types/index']],
                ['label' => 'Создать тип товара', 'url' => ['/admin/good-types/create']]
            ]
        ],
        [
            'label' => 'Сообщения с сайта',
            'url'   => ['/admin/callbacks']
        ],
        [
            'label' => 'Настройки',
            'url' => ['/admin/settings'],
            /*'items' => [
                ['label' => 'Настройки', 'url' => ['/admin/settings']],
                ['label' => 'Добавить настройку', 'url' => ['/admin/settings/create']]
            ]*/
        ],
        [
            'label'       => 'Logout (' . Yii::$app->user->identity->username . ')',
            'url'         => ['/admin/user/logout'],
            'linkOptions' => ['data-method' => 'post']
        ]
    ]
]);
NavBar::end();
