<?php
use yii\widgets\Breadcrumbs;

$this->beginContent('@app/modules/admin/views/layouts/base.php'); ?>
    <div class="col-md-4 col-md-offset-4">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
<?php $this->endContent();
