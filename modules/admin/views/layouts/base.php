<?php
use yii\helpers\Html;
use app\modules\admin\assets\AdminAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AdminAsset::register($this);

$actionWithoutNav = [
    'login',
    'signup',
    'request-password-reset',
    'reset-password'
];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        if ($this->context->id !== 'user' || !in_array($this->context->action->id, $actionWithoutNav)) {
            echo $this->renderFile('@admin/views/layouts/_navigation.php');

        } ?>
        <div class="container-fluid">
            <div class="row text-center">
                <?= $content ?>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?= Yii::$app->name . ' ' . date('Y') ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
