<?php
use yii\widgets\Breadcrumbs;

$this->beginContent('@app/modules/admin/views/layouts/base.php'); ?>
    <div class="col-md-10 col-md-offset-1" style="margin-top: 100px">
        <div class="text-left">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <?= $content ?>
    </div>
<?php $this->endContent();
