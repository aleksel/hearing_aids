<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Features */

$this->title = Yii::t('admin', 'Create {modelClass}', [
    'modelClass' => Yii::t('admin', '{n, plural, =0{Feature Type} =1{Feature Type} =2{Feature Types} other{Feature Types}}', ['n' => 2]),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Features'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="features-create">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>