<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GoodTypes */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('admin', '{n, plural, =0{Good Type} =1{Good Type} =2{Good Types} other{Good Types}}', ['n' => 2]),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Good Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update breadcrumbs');
?>
<div class="good-types-update">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>