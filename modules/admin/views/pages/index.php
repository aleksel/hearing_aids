<?php

use dosamigos\grid\ToggleColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Pages');
?>
<div class="pages-index">
    <?php Pjax::begin(); ?>
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-left">
            <?=
            Html::a(
                Yii::t('admin', Yii::t('admin', 'Create')),
                ['create'],
                ['class' => 'btn btn-success']
            );
            ?>
        </p>

        <p class="text-right">
            <?php
            // Количество элементов в гриде
            echo \nterms\pagesize\PageSize::widget(
                [
                    'label'   => Yii::t('admin', 'Items count'),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                    ]
                ]
            );
            ?>
        </p>
        <?= GridView::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'columns'        => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                'title',
                [
                    'attribute' => 'content',
                    'format'    => 'html',
                    'value'     => function ($model, $index, $widget) {
                        return mb_strlen($model->content) > 200 ? mb_substr($model->content, 0, 200) . ' .....' : $model->content;
                    }
                ],
                [
                    'attribute' => 'url',
                    'format'    => 'url',
                    'value'     => function ($model, $index, $widget) {
                        return $model->url ? ($_SERVER['HTTP_HOST'] . '/' . $model->url) : null;
                    }
                ],
                [
                    'class'          => ToggleColumn::className(),
                    'attribute'      => 'is_showing',
                    'onValue'        => "1",
                    'onLabel'        => 'Да',
                    'offLabel'       => 'Нет',
                    'contentOptions' => ['class' => 'text-center'],
                    //'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                    'filter'         => [1 => "Да", 0 => "Нет"],
                ],
                /*'meta_title',
                'meta_description',
                'meta_keywords',*/
                [
                    'attribute' => 'created_at',
                    'value'     => function ($model, $index, $widget) {
                        return $model->created_at ? date(Yii::$app->params['dateFormat'], $model->created_at) : null;
                    }
                ],
                [
                    'attribute' => 'updated_at',
                    'value'     => function ($model, $index, $widget) {
                        return $model->updated_at ? date(Yii::$app->params['dateFormat'], $model->updated_at) : null;
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
