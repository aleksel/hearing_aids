<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zhuravljov\widgets\DateTimePicker;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */

$model->created_at = date(Yii::$app->params['dateFormat'], $model->created_at ?: time());
if ($model->scenario !== 'create') {
    $model->updated_at = date(Yii::$app->params['dateFormat'], $model->updated_at ?: time());
}
?>

<div class="pages-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'content')
            ->widget(
                CKEditor::className(),
                [
                    'editorOptions' => ElFinder::ckeditorOptions(
                        'admin/elfinder',
                        [
                            'allowedContent' => true,
                            'height'         => 600,
                            'preset'         => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline'         => false, //по умолчанию false
                        ]
                    )
                ]
            ); ?>

        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'is_showing')->dropDownList(['1' => 'Да', '0' => 'Нет']/*, ['prompt' => '']*/) ?>

        <?= $form->field($model, 'created_at')->widget(DateTimePicker::className(), [
            'options'       => ['class' => 'form-control'],
            'clientOptions' => [
                'format'         => 'dd.mm.yyyy hh:ii',
                'language'       => 'ru',
                'autoclose'      => true,
                'todayHighlight' => true,
            ],
            'clientEvents'  => [],
        ]) ?>

        <?= $form->field($model, 'updated_at')->widget(DateTimePicker::className(), [
            'options'       => ['class' => 'form-control'],
            'clientOptions' => [
                'format'         => 'dd.mm.yyyy hh:ii',
                'language'       => 'ru',
                'autoclose'      => true,
                'todayHighlight' => true,
            ],
            'clientEvents'  => [],
        ]) ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(
    '$(document).ready(function () {
        (function () {
            "use strict";

            var app = {
                initialize: function () {
                    $("#pages-url").translit("watch", "#pages-title");
                    this.setUpListeners();
                },

                setUpListeners: function () {
                    //$(document).on("keyup", "#pages-title", $.proxy(this.transliterateTitle, this));
                }/*,

                transliterateTitle: function () {
                }*/
            }
            app.initialize();
        }());
    });'
);
