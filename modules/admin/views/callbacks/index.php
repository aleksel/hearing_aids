<?php

use dosamigos\editable\Editable;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CallbacksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Callbacks');
?>
<div class="callbacks-index">
    <?php Pjax::begin(); ?>
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-right">
            <?php
            // Количество элементов в гриде
            echo \nterms\pagesize\PageSize::widget(
                [
                    'label'   => Yii::t('admin', 'Items count'),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                    ]
                ]
            );
            ?>
        </p>

        <?= GridView::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'columns'        => [
                ['class' => 'yii\grid\SerialColumn'],

                'phone',
                'email:email',
                'message:ntext',
                [
                    'attribute' => 'comment',
                    'format'    => 'raw',
                    'value'     => function ($model, $index, $widget) {
                        return Editable::widget([
                            'model'         => $model,
                            'attribute'     => 'comment',
                            'url'           => 'callbacks/comment',
                            'type'          => 'wysihtml5',
                            'mode'          => 'pop',
                            'clientOptions' => [
                                'placement' => '',
                            ]
                        ]);
                    }
                ],
                [
                    'attribute'          => 'created_at',
                    'value'              => function ($model, $index, $widget) {
                        return $model->created_at ? date(Yii::$app->params['dateFormat'], $model->created_at) : null;
                    },
                    'filterInputOptions' => ['class' => 'form-control', /*'class'=>'select-multiple'*/],
                ],
                [
                    'attribute' => 'updated_at',
                    'value'     => function ($model, $index, $widget) {
                        return $model->updated_at ? date(Yii::$app->params['dateFormat'], $model->updated_at) : null;
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
