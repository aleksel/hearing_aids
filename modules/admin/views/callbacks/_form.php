<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Callbacks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="callbacks-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => 15]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => 15]) ?>

        <?= $form->field($model, 'message')->textarea(['maxlength' => 255]) ?>

        <?= $form->field($model, 'comment')->textarea(['maxlength' => 255]) ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
