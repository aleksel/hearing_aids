<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Callbacks */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => 'Callbacks',
    'modelClass' => Yii::t('admin', '{n, plural, =0{Callback} =1{Callback} =2{Callbacks} other{Callbacks}}', ['n' => 2]),
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Callbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update breadcrumbs');
?>
<div class="callbacks-update">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>