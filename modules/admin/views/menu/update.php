<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Menu */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('admin', 'Update {modelClass}', [
    'modelClass' => Yii::t('admin', 'Menu'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Menu'), 'url' => ['view']];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update breadcrumbs');
?>
<div class="user-update">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="menu-form text-left admin-form-crud">

            <?php $form = ActiveForm::begin(); ?>
            <div class="form-group text-left">
                <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="admin-form-crud-inside">
                <?php echo $form->errorSummary($model); ?>

                <?= $form->field($model, 'menu')->textarea(['style' => 'height: 1000px;']) ?>

            </div>

            <div class="form-group text-left">
                <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>