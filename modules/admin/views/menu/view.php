<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Menu */

?>
<div class="user-view">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-left">
            <?= Html::a(Yii::t('admin', 'Update'), ['/admin/menu/update'], ['class' => 'btn btn-primary']) ?>
        </p>

        <div class="header navbar-fixed-top">
            <div class="header-wrap">
                <div class="header-overflow font_main">
                    <div class="header-contact-outer col-md-offset-2">
                        <div class="header-contact">
                            <span class="header-metro-marker"></span>
                            <span class="header-metro font_main">
                                <a href="#map-contact"><?= @\Yii::$app->params['settings']['headerMetro']; ?></a>
                            </span>
                            <span class="header-phone-code font_main">
                                <?= @\Yii::$app->params['settings']['headerPhoneCode']; ?>
                            </span>
                            <span class="header-phone font_main">
                                <?= @\Yii::$app->params['settings']['headerPhone']; ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="header-menu">
                    <?= $this->renderFile('@app/views/layouts/_navigation.php'); ?>
                </div>
            </div>
        </div>
        <p class="text-left" style="margin-top: 100px">
            <pre class="col-md-6 col-md-offset-3">
                <code>
                    <?php highlight_string($model->getMenu()) ?>
                </code>
            </pre>
        </p>
        <div style="clear: both"></div>
        <p class="text-left">
            <?= Html::a(Yii::t('admin', 'Update'), ['/admin/menu/update'], ['class' => 'btn btn-primary']) ?>
        </p>
    </div>
</div>