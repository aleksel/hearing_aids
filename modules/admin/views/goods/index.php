<?php

use app\models\GoodTypes;
use dosamigos\grid\ToggleColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Goods');
?>

<div class="goods-index">
    <?php Pjax::begin(); ?>
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-left">
            <?=
            Html::a(
                Yii::t('admin', Yii::t('admin', 'Create')),
                ['create'],
                ['class' => 'btn btn-success']
            );
            ?>
        </p>

        <p class="text-right">
            <?php
            // Количество элементов в гриде
            echo \nterms\pagesize\PageSize::widget(
                [
                    'label'   => Yii::t('admin', 'Items count'),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                    ]
                ]
            );
            ?>
        </p>

        <?php echo GridView::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'columns'        => [
                [
                    'attribute' => 'label',
                    'format' => 'raw',
                    'value' => function ($model, $index, $widget) {
                        return Html::a($model->label, '/admin/goods/view?id=' . $model->id);
                    }
                ],
                [
                    'attribute' => 'url',
                    'format'    => 'url',
                    'value'     => function ($model, $index, $widget) {
                        return $model->url ? ($_SERVER['HTTP_HOST'] . '/catalog/' . $model->url) : null;
                    }
                ],
                [
                    'attribute' => 'type',
                    'filter'    => ArrayHelper::map(GoodTypes::find()->all(), 'id', 'name'),
                    'value' => function ($model, $index, $widget) {
                        return @$model->type0->name;
                    }
                ],
                'price',
                'comment:ntext',
                /*'meta_title',
                'meta_description',
                'meta_keywords',*/
                [
                    'class'          => ToggleColumn::className(),
                    'attribute'      => 'is_showing',
                    'onValue'        => "1",
                    'onLabel'        => 'Да',
                    'offLabel'       => 'Нет',
                    'contentOptions' => ['class' => 'text-center'],
                    //'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                    'filter'         => [1 => "Да", 0 => "Нет"],
                ],
                [
                    'class'          => ToggleColumn::className(),
                    'attribute'      => 'is_showing_popular',
                    'onValue'        => "1",
                    'onLabel'        => 'Да',
                    'offLabel'       => 'Нет',
                    'contentOptions' => ['class' => 'text-center'],
                    //'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                    'filter'         => [1 => "Да", 0 => "Нет"],
                ],
                [
                    'class'          => 'yii\grid\ActionColumn',
                    /*'urlCreator'     => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;

                        return \yii\helpers\Url::toRoute($params);
                    },*/
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
