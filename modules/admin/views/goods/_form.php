<?php

use app\models\Files;
use app\models\GoodTypes;
use kartik\widgets\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\Goods */
/* @var $form yii\widgets\ActiveForm */
$model->change_image = null;
$model->change_image_small = null;
$model->change_instruction = null;

?>

<div class="goods-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>
        <?php
        if (!$model->getIsNewRecord() && @$filesModel) {
            echo $form->errorSummary($filesModel);
        }
        ?>
        <?php
        if (!$model->getIsNewRecord() && @$goodsHasFilesModel) {
            echo $form->errorSummary($goodsHasFilesModel);
        }
        ?>

        <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(GoodTypes::find()->all(), 'id', 'name')) ?>
        <?= $form->field($model, 'price')->textInput() ?>
        <?= $form->field($model, 'label')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'image_small[]')->widget(FileInput::className(), [
            'model'         => $model,
            'attribute'     => 'image_small[]',
            'options'       => [
                'multiple'                                        => true,
                'accept'                                          => 'image/*',
                ($model->scenario !== 'create' ? 'disabled' : '') => ($model->scenario !== 'create' ? 'disabled' : '')
            ],
            'pluginOptions' => [
                //'overwriteInitial'=>false,
                'previewFileType' => 'image',
                'initialPreview'  => $model->getFileForForm(Files::TYPE_IMAGE_SMALL_GOOD),
                //'showPreview' => false,
                /*'showCaption' => true,
                'showRemove' => true,*/
                'showUpload'      => false
            ],
        ]); ?>
        <?php
        if ($model->scenario !== 'create') {
            echo $form->field($model, 'change_image_small')->checkbox(['maxlength' => 150]);
        }
        ?>
        <?= $form->field($model, 'image[]')->widget(FileInput::className(), [
            'model'         => $model,
            'attribute'     => 'image[]',
            'options'       => ['multiple' => true, 'accept' => 'image/*', ($model->scenario !== 'create' ? 'disabled' : '') => ($model->scenario !== 'create' ? 'disabled' : '')],
            'pluginOptions' => [
                //'overwriteInitial'=>false,
                'previewFileType' => 'any',
                'initialPreview'  => $model->getFileForForm(Files::TYPE_IMAGE_GOOD),
                //'showPreview' => false,
                /*'showCaption' => true,
                'showRemove' => true,*/
                'showUpload'      => false
            ],

        ]) ?>
        <?php
        if ($model->scenario !== 'create') {
            echo $form->field($model, 'change_image')->checkbox(['maxlength' => 150]);
        }
        ?>
        <?= $form->field($model, 'instruction')->widget(FileInput::className(), [
            'model'         => $model,
            'attribute'     => 'instruction',
            'options'       => [
                'multiple'                                        => false,
                'accept'                                          => 'any',
                ($model->scenario !== 'create' ? 'disabled' : '') => ($model->scenario !== 'create' ? 'disabled' : '')
            ],
            'pluginOptions' => [
                //'overwriteInitial'=>false,
                'previewFileType' => 'any',
                'initialPreview'  => $model->getFileForForm(Files::TYPE_INSTRUCTION_GOOD),
                //'showPreview' => false,
                /*'showCaption' => true,
                'showRemove' => true,*/
                'showUpload'      => false
            ],
        ]); ?>
        <?php
        if ($model->scenario !== 'create') {
            echo $form->field($model, 'change_instruction')->checkbox(['maxlength' => 150]);
        }
        ?>

        <?= $form->field($model, 'description_short')
            ->widget(
                CKEditor::className(),
                [
                    'editorOptions' => ElFinder::ckeditorOptions(
                        'admin/elfinder',
                        [
                            'allowedContent' => true,
                            'height'         => 600,
                            'preset'         => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline'         => false, //по умолчанию false
                        ]
                    )
                ]
            ); ?>
        <?= $form->field($model, 'description_full')
            ->widget(
                CKEditor::className(),
                [
                    'editorOptions' => ElFinder::ckeditorOptions(
                        'admin/elfinder',
                        [
                            'allowedContent' => true,
                            'height'         => 600,
                            'preset'         => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline'         => false, //по умолчанию false
                        ]
                    )
                ]
            ); ?>
        <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'is_showing')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        <?= $form->field($model, 'is_showing_popular')->dropDownList(['0' => 'Нет', '1' => 'Да']) ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
if ($model->scenario !== 'create') {
    $this->registerJs(
        '$(document).ready(function () {
            (function () {
                "use strict";

                var app = {
                    initialize: function () {
                        this.setUpListeners();
                        $("#goods-url").translit("watch", "#goods-label");
                    },

                    setUpListeners: function () {
                        $(document).on("change", "#goods-change_image", $.proxy(this.toggleField, this, "image"));
                        $(document).on("change", "#goods-change_image_small", $.proxy(this.toggleField, this, "image_small"));
                        $(document).on("change", "#goods-change_instruction", $.proxy(this.toggleField, this, "instruction"));
                    },

                    toggleField: function (field) {
                        $(".field-goods-" + field + " .kv-fileinput-caption").toggleClass("file-caption-disabled");
                        var buttons = $(".field-goods-" + field + " .fileinput-remove-button")
                            .add(".field-goods-" + field + " .btn-file").add("#goods-" + field);
                        buttons.attr("disabled", !buttons.attr("disabled"));
                    },

                }
                app.initialize();
            }());
        });'
    );
} else {
    $this->registerJs(
        '$(document).ready(function () {
            (function () {
                "use strict";

                var app = {
                    initialize: function () {
                        this.setUpListeners();
                        $("#goods-url").translit("watch", "#goods-label");
                    },

                    setUpListeners: function () {
                    }
                }
                app.initialize();
            }());
        });'
    );
}