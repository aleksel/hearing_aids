<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Goods */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('admin', '{n, plural, =0{Good} =1{Good} =2{Goods} other{Goods}}', ['n' => 2]),
    ]) . ': ' . $model->label;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Goods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update breadcrumbs');
?>
<div class="goods-update">

    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
