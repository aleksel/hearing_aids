<?php

use app\models\Files;
use dosamigos\gallery\Gallery;
use dosamigos\grid\ToggleColumn;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \yii\data\ActiveDataProvider $relatedGoodsProvider */
/* @var $model app\models\Goods */

$this->title = $model->label;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Goods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-view">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>


        <?php // -------------------------------- Товар ------------------------------------- ?>
        <?php $this->beginBlock('app\models\Goods'); ?>
        <hr/>
        <p class="text-left">
            <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]) ?>
        </p>
        <?php
        $model->trigger($model::EVENT_FIND);
        ?>
        <?= DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'id',
                'type',
                'label',
                [
                    'attribute' => 'url',
                    'format' => 'url',
                    'value' =>  $model->url ? ($_SERVER['HTTP_HOST'] . '/catalog/' . $model->url) : null
                ],
                'price',
                'description_short:html',
                'description_full:html',
                'comment:ntext',
                [
                    'attribute' => 'image',
                    'format'    => 'raw',
                    'value'     => $model->image_gallery ? Gallery::widget(['items' => $model->image_gallery]) : null
                ],
                [
                    'attribute' => 'image_small',
                    'format'    => 'raw',
                    'value'     => $model->image_small_gallery ?
                        Gallery::widget([
                            'items'           => $model->image_small_gallery,
                            'templateOptions' => ['id' => $model->image_gallery ? 'blueimp-gallery2' : 'blueimp-gallery'],
                            'clientOptions'   => ['container' => $model->image_gallery ?  'blueimp-gallery2' : 'blueimp-gallery']
                        ]) : null
                ],
                [
                    'attribute' => 'instruction',
                    'format'    => 'raw',
                    'value'     => current($model->getFileForForm(Files::TYPE_INSTRUCTION_GOOD))
                        ? (current($model->getFileForForm(Files::TYPE_INSTRUCTION_GOOD)) . Html::a($model->instruction, $model->instruction))
                        : null
                ],
                'is_showing:boolean',
                'is_showing_popular:boolean',
                'meta_title:ntext',
                'meta_description:ntext',
                'meta_keywords:ntext',
            ],
        ]); ?>
        <p class="text-left">
            <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?php // -------------------------------- Характеристики ------------------------------------- ?>
        <?php $this->beginBlock('app\models\Features'); ?>
        <p class='pull-right'>
            <?=
            Html::a(
                Yii::t('admin', 'List All Goods Features'),
                ['features/index'],
                ['class' => 'btn text-muted btn-xs']
            );
            ?>
            <?=
            Html::a(
                Yii::t('admin', 'New Goods Feature'),
                ['features/create', 'GoodsHasFeatures' => ['good_id' => $model->id]],
                ['class' => 'btn btn-success btn-xs']
            );
            ?>
        </p>

        <div class='clearfix'></div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout'       => "{items}\n{pager}",
            'columns'      => [
                [
                    'attribute' => 'feature_id',
                    'header'    => Yii::t('admin', 'feature_id'),
                    'format'    => 'html',
                    'value'     => function ($model, $index, $widget) {
                        return Html::a(@$model->feature->name, '/admin/features/update?id=' . $model->id);
                    }
                ],
                [
                    'attribute' => 'value',
                    'header'    => Yii::t('admin', 'value')
                ],
                [
                    'class'          => ToggleColumn::className(),
                    'url'            => ['/admin/features/toggle'],
                    'attribute'      => 'is_showing',
                    'header'         => Yii::t('admin', 'is_showing'),
                    'onValue'        => "1",
                    'onLabel'        => 'Да',
                    'offLabel'       => 'Нет',
                    'contentOptions' => ['class' => 'text-center'],
                    //'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                    'filter'         => [1 => "Да", 0 => "Нет"],
                ],
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'template'       => '{update} {delete}',
                    'urlCreator'     => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = 'features/' . $action;

                        return Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
        <?php $this->endBlock() ?>


        <?php // -------------------------------- Сопутствующие товары ------------------------------------- ?>
        <?php $this->beginBlock('app\models\GoodsHasGoods'); ?>
        <p class='pull-right'>
            <?=
            Html::a(
                Yii::t('admin', 'New Related Good'),
                ['related-goods/create', 'GoodsHasGoods' => ['good_id_f' => $model->id]],
                ['class' => 'btn btn-success btn-xs']
            );
            ?>
        </p>

        <div class='clearfix'></div>

        <?= GridView::widget([
            'dataProvider' => $relatedGoodsProvider,
            //'filterModel' => $searchModel,
            'layout'       => "{items}\n{pager}",
            'columns'      => [
                [
                    'attribute' => '',
                    'header'    => Yii::t('admin', 'label'),
                    'format'    => 'html',
                    'value'     => function ($model, $index, $widget) {
                        return Html::a($model->goodsS->label, '/admin/goods/view?id=' . $model->good_id_s);
                    }
                ],
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'template'       => '{delete}',
                    'urlCreator'     => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = 'related-goods/' . $action;

                        return Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
        <?php $this->endBlock() ?>

        <?= Tabs::widget([
            'id'           => 'relation-tabs',
            'encodeLabels' => false,
            'items'        => [
                [
                    'label'   => Yii::t('admin', 'Goods Tab'),
                    'content' => $this->blocks['app\models\Goods'],
                    'active'  => true,
                ],
                [
                    'label'   => Yii::t('admin', 'Goods Features Tab'),
                    'content' => $this->blocks['app\models\Features'],
                    'active'  => false,
                ],
                [
                    'label'   => Yii::t('admin', 'Related Goods'),
                    'content' => $this->blocks['app\models\GoodsHasGoods'],
                    'active'  => false,
                ],
            ]
            ]); ?>
    </div>
</div>
