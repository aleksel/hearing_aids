<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\GoodsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="goods-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'type') ?>

		<?= $form->field($model, 'label') ?>

		<?= $form->field($model, 'price') ?>

		<?= $form->field($model, 'description_short') ?>

		<?php // echo $form->field($model, 'description_full') ?>

		<?php // echo $form->field($model, 'comment') ?>

		<?php // echo $form->field($model, 'image_small') ?>

		<?php // echo $form->field($model, 'image') ?>

		<?php // echo $form->field($model, 'instruction') ?>

		<?php // echo $form->field($model, 'is_showing') ?>

		<?php // echo $form->field($model, 'is_showing_popular') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
