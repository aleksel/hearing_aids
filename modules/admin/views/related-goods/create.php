<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GoodsHasGoods */

$this->title = Yii::t('admin', 'Create {modelClass}', [
    'modelClass' => 'Goods Has Goods',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Goods Has Goods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-has-goods-create">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model'          => $model,
            'existsRelation' => $existsRelation
        ]) ?>
    </div>
</div>