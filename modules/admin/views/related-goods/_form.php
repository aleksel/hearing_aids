<?php

use app\models\Goods;
use app\models\GoodsHasGoods;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GoodsHasGoods */
/* @var $existsRelation app\models\GoodsHasGoods */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="goods-has-goods-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'good_id_s')
            ->dropDownList(
                ArrayHelper::map(
                    Goods::find()->where(($existsRelation ? "id NOT IN ($existsRelation)" : ''))->all(),
                    'id',
                    'label'
                )
            ); ?>

        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
