<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\user */

$this->title = Yii::t('admin', 'Create {modelClass}', [
    'modelClass' => Yii::t('admin', '{n, plural, =0{User} =1{User} =2{Users} other{Users}}', ['n' => 2]),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->generateAuthKey();
?>
<div class="user-create">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>
        <br/>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>