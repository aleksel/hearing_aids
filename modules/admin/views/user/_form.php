<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use zhuravljov\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\user */
/* @var $form yii\widgets\ActiveForm */

$model->created_at = date(Yii::$app->params['dateFormat'], $model->created_at ? : time());
if ($model->scenario !== 'create') {
    $model->updated_at = date(Yii::$app->params['dateFormat'], $model->updated_at ?: time());
}
$model->password_hash = '';
?>

<div class="user-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group text-left">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>

        <?= $form->field($model, 'nik')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'password_hash')
            ->passwordInput(
                [
                    'maxlength' => 150, 'disabled' => $model->scenario !== 'create' ? true : false
                ]
            )
        ?>
        <?php
        if ($model->scenario !== 'create') {
            echo $form->field($model, 'change_password')->checkbox(['maxlength' => 150]);
        }
        ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'role')
            ->dropDownList(
                User::$ROLE,
                ['maxlength' => 150]
            )
        ?>

        <?//= $form->field($model, 'reset_key')->textInput(['maxlength' => 255]) ?>

        <?//= $form->field($model, 'auth_key')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'status')
            ->dropDownList(
                User::$STATUS,
                ['maxlength' => 150]
            )
        ?>

        <?= $form->field($model, 'created_at')->widget(DateTimePicker::className(), [
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'format' => 'dd.mm.yyyy hh:ii',
                'language' => 'ru',
                'autoclose' => true,
                'todayHighlight' => true,
            ],
            'clientEvents' => [],
        ]) ?>

        <?= $form->field($model, 'updated_at')->widget(DateTimePicker::className(), [
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'format' => 'dd.mm.yyyy hh:ii',
                'language' => 'ru',
                'autoclose' => true,
                'todayHighlight' => true,
            ],
            'clientEvents' => [],
        ]) ?>

    </div>

    <div class="form-group text-left">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
if ($model->scenario !== 'create') {
    $this->registerJs(
        '$(document).ready(function () {
            (function () {
                "use strict";

                var app = {
                    initialize: function () {
                        this.setUpListeners();
                    },

                    setUpListeners: function () {
                        $(document).on("change", "#user-change_password", $.proxy(this.togglePasswordField, this));
                    },

                    togglePasswordField: function () {
                        var elem = $("#user-password_hash");
                        elem.attr("disabled", !elem.attr("disabled"));
                    }
                }
                app.initialize();
            }());
        });'
    );
}
