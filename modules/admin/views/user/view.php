<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username . ($model->nik ? (' (' . $model->nik .  ')') : '');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-left">
            <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'nik',
                'username',
                //'password_hash',
                'email:email',
                [
                    'attribute' => 'role',
                    'value' => \app\models\User::$ROLE[$model->role]
                ],
                'reset_key',
                'auth_key',
                [
                    'attribute' => 'status',
                    'value' => \app\models\User::$STATUS[$model->status]
                ],
                [
                    'attribute' => 'created_at',
                    'value' => date(Yii::$app->params['dateFormat'], $model->created_at),
                    'filterInputOptions' => ['class'=>'form-control', /*'class'=>'select-multiple'*/],
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => $model->updated_at ? date(Yii::$app->params['dateFormat'], $model->updated_at) : null,
                ],
            ],
        ]) ?>
        <p class="text-left">
            <?= Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('admin', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>
</div>