<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Users');
?>
<div class="user-index">
    <?php Pjax::begin(); ?>
        <div id="grid_content">
            <h1><?= Html::encode($this->title) ?></h1>
            <p class="text-left">
                <?=
                Html::a(
                    Yii::t('admin', Yii::t('admin', 'Create')),
                    ['create'],
                    ['class' => 'btn btn-success']
                );
                ?>
            </p>
            <p class="text-right">
                <?php
                // Количество элементов в гриде
                echo \nterms\pagesize\PageSize::widget(
                    [
                        'label' => Yii::t('admin', 'Items count'),
                        'options' => [
                            'class' => 'form-control',
                            'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                        ]
                    ]
                );
                ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="per-page"]',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    /*[
                        'class' => 'yii\grid\CheckboxColumn',
                        // you may configure additional properties here
                    ],*/
                    /*'columns' => [
                        'id',
                        //'type',
                        [
                            'attribute' => 'section',
                            'filter' => ArrayHelper::map(
                                Setting::find()->select('section')->distinct()->where('section <> ""')->all(),
                                'section',
                                'section'
                            ),
                        ],
                        'key',
                        'value:ntext',
                        [
                            'class' => '\pheme\grid\ToggleColumn',
                            'attribute' => 'active',
                            'filter' => [1 => Yii::t('yii', 'Yes'), 0 => Yii::t('yii', 'No')],
                        ],
                        ['class' => 'yii\grid\ActionColumn'],
                    ],*/
                    //'id',
                    'nik',
                    'username',
                    // 'password_hash',
                    'email:email',
                    [
                        'attribute' => 'role',
                        'value' => function ($model, $index, $widget, $gf) {
                            return User::$ROLE[$model->role];
                        },
                        'filter' => User::$ROLE
                    ],
                    // 'reset_key',
                    // 'auth_key',
                    [
                        'attribute' => 'status',
                        'value' => function ($model, $index, $widget) {
                            return User::$STATUS[$model->status];
                        },
                        'filter' => User::$STATUS
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model, $index, $widget) {
                            return $model->created_at ? date(Yii::$app->params['dateFormat'], $model->created_at) : null;
                        },
                        'filterInputOptions' => ['class'=>'form-control', /*'class'=>'select-multiple'*/],
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function ($model, $index, $widget) {
                            return $model->updated_at ? date(Yii::$app->params['dateFormat'], $model->updated_at) : null;
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    <?php Pjax::end(); ?>
</div>