<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

$this->title = Yii::t('admin', 'Enter the control panel');
?>
<div class="admin-login">
    <h1>
        <span class="glyphicon glyphicon-lock uk-icon-medium"></span>
        <?= Html::encode($this->title) ?>
    </h1>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 text-left">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <div style="color:#999;margin:1em 0">
                <?=
                Yii::t(
                    'admin',
                    'If you forgot your password you can '
                ) . Html::a(Yii::t('admin', 'reset it'), ['/admin/user/request-password-reset']);
                ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('admin', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
