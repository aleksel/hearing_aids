<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\ResetPasswordForm */

$this->title = Yii::t('admin', 'Reset password');
?>
<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    echo \app\components\widgets\Alert::widget();
    ?>
    <p><?= Yii::t('admin', 'Please choose your new password:'); ?></p>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 text-left">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
