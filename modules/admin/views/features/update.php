<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GoodsHasFeatures */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('admin', '{n, plural, =0{Feature} =1{Feature} =2{Features} other{Features}}', ['n' => 2]),
]) . ': ' . $model->feature->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Goods Has Features'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update breadcrumbs');
?>
<div class="goods-has-features-update">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>