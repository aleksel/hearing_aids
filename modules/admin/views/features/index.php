<?php

use app\models\Features;
use dosamigos\grid\ToggleColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GoodsHasFeaturesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Goods Has Features');
?>
<div class="goods-has-features-index">
    <?php Pjax::begin(); ?>
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-right">
            <?php
            // Количество элементов в гриде
            echo \nterms\pagesize\PageSize::widget(
                [
                    'label'   => Yii::t('admin', 'Items count'),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                    ]
                ]
            );
            ?>
        </p>

        <?= GridView::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'columns'        => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'good_id',
                    //'filter'    => ArrayHelper::map(\app\models\Goods::find()->all(), 'id', 'name'),
                    'format'    => 'raw',
                    'value'     => function ($model, $index, $widget) {
                        return Html::a(@$model->good->label, '/admin/goods/view?id=' . $model->good_id);
                    }
                ],
                [
                    'attribute' => 'feature_id',
                    'filter'    => ArrayHelper::map(Features::find()->all(), 'id', 'name'),
                    'value'     => function ($model, $index, $widget) {
                        return @$model->feature->name;
                    }
                ],
                'value',
                [
                    'class'          => ToggleColumn::className(),
                    'attribute'      => 'is_showing',
                    'onValue'        => "1",
                    'onLabel'        => 'Да',
                    'offLabel'       => 'Нет',
                    'contentOptions' => ['class' => 'text-center'],
                    //'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                    'filter'         => [1 => "Да", 0 => "Нет"],
                ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>

