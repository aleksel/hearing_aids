<?php

use app\models\Features;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GoodsHasFeatures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-has-features-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>
        <?= $form->field($model, 'feature_id')->dropDownList(ArrayHelper::map(Features::find()->all(), 'id', 'name')) ?>

        <?= $form->field($model, 'value')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'is_showing')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
