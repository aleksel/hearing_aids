<?php

use app\models\Banners;
use dosamigos\grid\ToggleColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BannerPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Banner Pages');
?>
<div class="banner-pages-index">
    <?php Pjax::begin(); ?>
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-left">
            <?=
            Html::a(
                Yii::t('admin', Yii::t('admin', 'Create')),
                ['create'],
                ['class' => 'btn btn-success']
            );
            ?>
        </p>

        <p class="text-right">
            <?php
            // Количество элементов в гриде
            echo \nterms\pagesize\PageSize::widget(
                [
                    'label'   => Yii::t('admin', 'Items count'),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                    ]
                ]
            );
            ?>
        </p>
        <?= GridView::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'columns'        => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'banner_id',
                    'filter'    => ArrayHelper::map(Banners::find()->all(), 'id', 'label'),
                    'value'     => function ($model, $index, $widget) {
                        return @$model->banner->label ?: @$model->banner->name;
                    }
                ],
                [
                    'attribute' => 'content',
                    'format'    => 'html',
                    'value'     => function ($model, $index, $widget) {
                        return ('<div class="admin-banner" style="">' . $model->content . '</div>');
                    }
                ],
                [
                    'class' => ToggleColumn::className(),
                    'attribute' => 'is_showing',
                    'onValue' => "1",
                    'onLabel' => 'Да',
                    'offLabel' => 'Нет',
                    'contentOptions' => ['class' => 'text-center'],
                    //'afterToggle' => 'function(r, data){if(r){console.log("done", data)};}',
                    'filter'    => [1 => "Да", 0 => "Нет"],
                ],
                'order',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
