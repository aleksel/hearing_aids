<?php

use app\models\Banners;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\BannerPages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-pages-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>

        <?= $form->field($model, 'banner_id')->dropDownList(ArrayHelper::map(Banners::find()->all(), 'id', 'label'), ['prompt' => '']); ?>

        <?= $form->field($model, 'content')
            ->widget(
                CKEditor::className(),
                [
                    'editorOptions' => ElFinder::ckeditorOptions(
                        'admin/elfinder',
                        [
                            'allowedContent' => true,
                            'height'         => 600,
                            'preset'         => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline'         => false, //по умолчанию false
                        ]
                    )
                ]
            ); ?>

        <?= $form->field($model, 'is_showing')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>

        <?= $form->field($model, 'order')->textInput() ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
