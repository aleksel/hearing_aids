<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BannerPages */

$this->title = Yii::t('admin', 'Update {modelClass}: ', [
'modelClass' => Yii::t('admin', '{n, plural, =0{Banner Page} =1{Banner Page} =2{Banner Pages} other{Banner Pages}}', ['n' => 2]),
]) . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Banner Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('admin', 'Update breadcrumbs');
?>
<div class="banner-pages-update">
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
