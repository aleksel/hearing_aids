<?php

use app\models\Settings;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Settings');
?>
<div class="settings-index">
    <?php Pjax::begin(); ?>
    <div id="grid_content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="text-right">
            <?php
            // Количество элементов в гриде
            echo \nterms\pagesize\PageSize::widget(
                [
                    'label'   => Yii::t('admin', 'Items count'),
                    'options' => [
                        'class' => 'form-control',
                        'style' => 'width: 90px; float: right; margin: -7px 0 0 10px;'
                    ]
                ]
            );
            ?>
        </p>

        <?= GridView::widget([
            'dataProvider'   => $dataProvider,
            'filterModel'    => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            'columns'        => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                [
                    'attribute' => 'value',
                    'value'     => function ($model, $index, $widget) {
                        return mb_strlen($model->value) > 200 ? mb_substr($model->value, 0, 200) . ' .....' : $model->value;
                    }
                ],
                // 'value:ntext',
                'label',
                [
                    'attribute' => 'type',
                    'value'     => function ($model, $index, $widget, $gf) {
                        return Settings::$TYPE[$model->type];
                    },
                    'filter'    => Settings::$TYPE
                ],
                // 'edit',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>