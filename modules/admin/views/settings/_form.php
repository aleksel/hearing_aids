<?php

use app\models\Settings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form text-left admin-form-crud">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <div class="admin-form-crud-inside">
        <?php echo $form->errorSummary($model); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

        <?php
        switch ($model->type) {
            case (Settings::TYPE_STRING):
                echo $form->field($model, 'value')->textInput(['maxlength' => 255, 'rows' => 6]);
                break;
            case (Settings::TYPE_CHECKBOX):
                echo $form->field($model, 'value')->checkbox(['rows' => 6]);
                break;
            default:
                echo $form->field($model, 'value')
                    ->widget(
                        CKEditor::className(),
                        [
                            'editorOptions' => [
                                'allowedContent' => true,
                                'height'         => 600,
                                'preset'         => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                'inline'         => false, //по умолчанию false
                            ],
                        ]
                    );
                //echo $form->field($model, 'value')->textarea(['rows' => 6]);
                break;
        }
        ?>

        <?= $form->field($model, 'label')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($model, 'type')
            ->dropDownList(
                Settings::$TYPE,
                ['maxlength' => 150]
            )
        ?>

        <? //= $form->field($model, 'edit')->checkbox() ?>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('admin', 'Create') : Yii::t('admin', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
