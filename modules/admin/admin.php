<?php

namespace app\modules\admin;

use app\models\Settings;
use yii\di\Container;

class admin extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';
    public $defaultRoute = 'user';

    public function init()
    {
        parent::init();
        \Yii::setAlias('admin', \Yii::getAlias('@app') . DS . 'modules' . DS . 'admin');
        \Yii::configure($this, require(__DIR__ . '/config.php'));
        /*if ($dateFormat = Settings::find()->select('value')->where(['name' => 'dateFormat'])->one()->value) {
            \Yii::$app->params['dateFormat'] = $dateFormat;
        }*/
    }

    public $controllerMap = [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['@'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
            'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
            'roots' => [
                [
                    'baseUrl'=>'@admin',
                    'basePath'=>'@webroot',
                    'path' => 'files/global',
                    'name' => 'Global'
                ],
                [
                    'class' => 'mihaildev\elfinder\UserPath',
                    'path'  => 'files/user_{id}',
                    'name'  => 'My Documents'
                ],
                [
                    'path' => 'files/some',
                    'name' => ['category' => 'my','message' => 'Some Name'] //перевод Yii::t($category, $message)
                ]/*,
                [
                    'path'   => 'files/some',
                    'name'   => ['category' => 'my','message' => 'Some Name'], // Yii::t($category, $message)
                    'access' => ['read' => '*', 'write' => 'UserFilesAccess'] // * - для всех, иначе проверка доступа в даааном примере все могут видет а редактировать могут пользователи только с правами UserFilesAccess
                ]*/
            ]
        ]
    ];
}
