<?php

namespace app\modules\admin\controllers;

use app\models\Pages;
use Yii;
use app\models\GoodsHasGoods;
use app\models\GoodsHasGoodsSearch;
use app\modules\admin\controllers\AdminController;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RelatedGoodsController implements the CRUD actions for GoodsHasGoods model.
 */
class RelatedGoodsController extends AdminController
{
    /**
     * Creates a new GoodsHasGoods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @throws BadRequestHttpException
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GoodsHasGoods();
        if (!$good_id_f = @Yii::$app->request->get()['GoodsHasGoods']['good_id_f']) {
            throw new BadRequestHttpException('Bad request', 400);
        }
        $model->good_id_f = $good_id_f;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['/admin/goods/view', 'id' => $model->good_id_f]);
            }
        } else {
            $existsRelation = GoodsHasGoods::find()->where(['good_id_f' => $model->good_id_f])->all();
            if ($existsRelation) {
                $existsRelation = ArrayHelper::getColumn($existsRelation, 'good_id_s');
            }

            $existsRelation[] = $good_id_f;
            $existsRelation = implode(', ', $existsRelation);

            return $this->render('create', [
                'model'          => $model,
                'existsRelation' => $existsRelation
            ]);
        }
    }

    /**
     * Deletes an existing GoodsHasGoods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $good_id_f
     * @param integer $good_id_s
     * @return mixed
     */
    public function actionDelete($good_id_f, $good_id_s)
    {
        $model = $this->findModel($good_id_f, $good_id_s);
        $model->delete();

        return $this->redirect(['/admin/goods/view', 'id' => $model->good_id_f]);
    }

    /**
     * Finds the GoodsHasGoods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $good_id_f
     * @param integer $good_id_s
     * @return GoodsHasGoods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($good_id_f, $good_id_s)
    {
        if (($model = GoodsHasGoods::findOne(['good_id_f' => $good_id_f, 'good_id_s' => $good_id_s])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
