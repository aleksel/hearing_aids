<?php

namespace app\modules\admin\controllers;

use dosamigos\grid\ToggleAction;
use Yii;
use app\models\Features;
use app\models\FeaturesSearch;
use app\modules\admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeatureTypesController implements the CRUD actions for Features model.
 */
class FeatureTypesController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\Features';
        $modelSearchClass = 'app\models\FeaturesSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'create' => [
                'class'       => 'app\modules\admin\controllers\actions\CreateAction',
                'view'        => 'create',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/feature-types',
                'parameters'  => false
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\UpdateAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/feature-types',
                'parameters'  => false
            ],
            'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/feature-types'
            ],
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => $modelClass,
                'onValue' => '1',
                'offValue' => '0'
            ],
        ];

        return $actions + parent::actions();
    }
}
