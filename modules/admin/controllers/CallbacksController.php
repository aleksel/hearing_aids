<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Callbacks;
use app\models\CallbacksSearch;
use app\modules\admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CallbacksController implements the CRUD actions for Callbacks model.
 */
class CallbacksController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\Callbacks';
        $modelSearchClass = 'app\models\CallbacksSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\UpdateAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/callbacks/view'
            ],
            'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/callbacks'
            ],
            'comment' => [
                'class' => 'dosamigos\editable\EditableAction',
                //'scenario'=>'editable',  //optional
                'modelClass' => $modelClass,
            ],
        ];

        return $actions + parent::actions();
    }
}
