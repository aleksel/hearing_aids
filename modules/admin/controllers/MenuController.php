<?php
namespace app\modules\admin\controllers;

use app\modules\admin\models\Menu;
use Yii;

/**
 * Menu controller
 */
class MenuController extends AdminController
{
    public function actionUpdate()
    {
        /** @var Menu $model */
        $model = new Menu();
        //$model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/admin/menu']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionIndex()
    {
        return $this->render('view', [
            'model' => new Menu(),
        ]);
    }
}
