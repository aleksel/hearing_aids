<?php
namespace app\modules\admin\controllers;

use yii\base\UserException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\assets\AppAsset;

/**
 * Class AdminController
 * @package app\modules\admin\controllers
 */
class AdminController extends Controller
{
    public $currentMenu = 'settings';
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function init()
    {
        \Yii::$app->setHomeUrl(\Yii::$app->params['adminHomeUrl']);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'logout', 'error', 'signup', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return @\Yii::$app->user->identity->role === User::ROLE_ADMIN ? true : false;
                        }
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new UserException(\Yii::t('admin', 'You are not allowed to access this page'));
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    //'logout' => ['post'],
                ],
            ],
        ];
    }
}
