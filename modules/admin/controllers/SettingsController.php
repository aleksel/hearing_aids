<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Settings;
use app\models\SettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Settings Controller implements the CRUD actions for Settings model.
 */
class SettingsController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\Settings';
        $modelSearchClass = 'app\models\SettingsSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            /*'create' => [
                'class'       => 'app\modules\admin\controllers\actions\CreateAction',
                'view'        => 'create',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/user/view'
            ],*/
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\UpdateAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/settings/view'
            ],
            'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/settings'
            ],
        ];

        return $actions + parent::actions();
    }
}
