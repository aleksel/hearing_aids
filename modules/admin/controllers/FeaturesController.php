<?php

namespace app\modules\admin\controllers;

use dosamigos\grid\ToggleAction;
use Yii;
use app\models\GoodsHasFeatures;
use app\models\GoodsHasFeaturesSearch;
use app\modules\admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeaturesController implements the CRUD actions for GoodsHasFeatures model.
 */
class FeaturesController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\GoodsHasFeatures';
        $modelSearchClass = 'app\models\GoodsHasFeaturesSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'create' => [
                'class'       => 'app\modules\admin\controllers\actions\features\CreateFeaturesAction',
                'view'        => 'create',
                'modelClass'  => $modelClass
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\features\UpdateFeaturesAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/features',
                'parameters'  => false
            ],
            /*'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],*/
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\features\DeleteFeaturesAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/features'
            ],
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => $modelClass,
                'onValue' => '1',
                'offValue' => '0'
            ],
        ];

        return $actions + parent::actions();
    }
}
