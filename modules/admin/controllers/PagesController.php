<?php

namespace app\modules\admin\controllers;

use dosamigos\grid\ToggleAction;
use Yii;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\Pages';
        $modelSearchClass = 'app\models\PagesSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'create' => [
                'class'       => 'app\modules\admin\controllers\actions\CreateAction',
                'view'        => 'create',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/pages/view'
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\UpdateAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/pages/view'
            ],
            'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/pages'
            ],
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => $modelClass,
                'onValue' => '1',
                'offValue' => '0'
            ],
        ];

        return $actions + parent::actions();
    }
}
