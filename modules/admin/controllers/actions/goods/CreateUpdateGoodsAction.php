<?php

namespace app\modules\admin\controllers\actions\goods;

use Yii;
use app\components\exceptions\FileExistsException;
use app\models\Files;
use app\models\Goods;
use app\models\GoodsHasFiles;
use app\modules\admin\controllers\actions\CAction;
use yii\base\Exception;
use yii\base\InvalidCallException;

/**
 * Class CreateUpdateGoodsAction
 * @package app\modules\admin\controllers\actions
 */
class CreateUpdateGoodsAction extends CAction
{
    public $view = 'update';
    public $redirectUrl = null;
    public $parameters = true;

    public function run($id = null)
    {
        if (!$this->modelClass || !$this->redirectUrl) {
            throw new InvalidCallException(Yii::t('admin', 'Error'), 500);
        }

        /** @var Goods $model */
        // Создание нового товара
        if ($id) {
            $model = $this->findModel($id);
            $model->setIsNewRecord(false);
        // Изменение существующего товара
        } else {
            $model = new Goods();
            $model->setIsNewRecord(true);
            $model->scenario = 'create';
        }

        $filesModel = new Files();
        $goodsHasFilesModel = new GoodsHasFiles();

        if ($model->load(Yii::$app->request->post())) {

            try {
                // Транзакция для сохранения всех файлов к товару
                $transaction = Yii::$app->getDb()->beginTransaction();

                if (!$model->save()) {
                    throw new Exception('Ошибка сохранения записи в таблицу goods', 500);
                }

                $path = Yii::getAlias('@webroot') . Yii::$app->params['goodsFilePath'];

                // Сохраняем картинки
                $model->saveFiles($model, $path, 'change_image', 'image', Files::TYPE_IMAGE_GOOD);
                // Сохраняем маленькие картинки
                $model->saveFiles($model, $path, 'change_image_small', 'image_small', Files::TYPE_IMAGE_SMALL_GOOD);
                // Сохраняем инструкции
                $model->saveFiles($model, $path, 'change_instruction', 'instruction', Files::TYPE_INSTRUCTION_GOOD);

                $url = [$this->redirectUrl];
                if ($this->parameters) {
                    $url['id'] = $model->id;
                }

                $transaction->commit();

                return $this->controller->redirect($url);

            } catch (FileExistsException $e) {

                $model->addError($e->attribute, $e->getMessage());

            } catch (Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        return $this->controller->render($this->view, [
            'model'              => $model,
            'filesModel'         => $filesModel,
            'goodsHasFilesModel' => $goodsHasFilesModel
        ]);
    }
}
