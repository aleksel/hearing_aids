<?php

namespace app\modules\admin\controllers\actions\goods;

use app\models\GoodsHasGoods;
use app\models\GoodsHasGoodsSearch;
use Yii;
use app\models\GoodsHasFeaturesSearch;
use app\modules\admin\controllers\actions\CAction;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * Class ViewWitchRelationsAction
 * @package app\modules\admin\controllers\actions
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
class ViewWithRelationsAction extends CAction
{
    public $view = 'view';
    public $modelSearchClass = null;

    public function run($id)
    {
        $dataProvider = new ActiveDataProvider(
            [
                'query'      => GoodsHasFeaturesSearch::find()->where(['good_id' => $id]),
                'pagination' => [
                    'pageSize' => 100,
                ]
            ]
        );

        $relatedGoodsProvider = new ActiveDataProvider(
            [
                'query'      => GoodsHasGoodsSearch::find()->where(['good_id_f' => $id])->joinWith('goodsS', true),
                'pagination' => [
                    'pageSize' => 100,
                ]
            ]
        );

        Url::remember();

        return $this->controller->render($this->view, [
            'model'                => $this->findModel($id),
            'dataProvider'         => $dataProvider,
            'relatedGoodsProvider' => $relatedGoodsProvider
        ]);
    }
}
