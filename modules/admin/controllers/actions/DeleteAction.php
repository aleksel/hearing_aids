<?php

namespace app\modules\admin\controllers\actions;

use Yii;

/**
 * Class DeleteAction
 * @package app\modules\admin\controllers\actions
 */
class DeleteAction extends CAction
{
    public $redirectUrl = null;

    public function run($id)
    {
        $this->findModel($id)->delete();
        return $this->controller->redirect([$this->redirectUrl]);
    }
}
