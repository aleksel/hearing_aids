<?php

namespace app\modules\admin\controllers\actions;

use Yii;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;

/**
 * Class UpdateAction
 * @package app\modules\admin\controllers\actions
 */
class UpdateAction extends CAction
{
    public $view = 'update';
    public $redirectUrl = null;
    public $parameters = true;

    public function run($id)
    {
        if (!$this->modelClass || !$this->redirectUrl) {
            throw new InvalidCallException(Yii::t('admin', 'Error'), 500);
        }

        /** @var ActiveRecord $model */
        $model = $this->findModel($id);
        $model->setIsNewRecord(false);
        //$model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = [$this->redirectUrl];
            if ($this->parameters) {
                $url['id'] = $model->id;
            }
            return $this->controller->redirect($url);
        } else {
            return $this->controller->render($this->view, [
                'model' => $model,
            ]);
        }
    }
}
