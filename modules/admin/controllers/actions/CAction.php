<?php

namespace app\modules\admin\controllers\actions;

use yii\rest\Action;

/**
 * Class CAction
 * @package app\modules\admin\controllers\actions
 */
class CAction extends Action
{
    public $modelClass = 'yii\db\ActiveRecord';

    public function init()
    {
        //$this->controller->layout = 'main';
        parent::init();
    }
}
