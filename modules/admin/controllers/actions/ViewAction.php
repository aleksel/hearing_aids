<?php

namespace app\modules\admin\controllers\actions;

use Yii;

/**
 * Class ViewAction
 * @package app\modules\admin\controllers\actions
 */
class ViewAction extends CAction
{
    public $view = 'view';

    public function run($id)
    {
        return $this->controller->render($this->view, [
            'model' => $this->findModel($id),
        ]);
    }
}
