<?php

namespace app\modules\admin\controllers\actions;

use Yii;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;

/**
 * Class CreateAction
 * @package app\modules\admin\controllers\actions
 */
class CreateAction extends CAction
{
    public $view = 'create';
    public $redirectUrl = null;
    public $parameters = true;

    public function run()
    {
        if (!$this->modelClass || !$this->redirectUrl) {
            throw new InvalidCallException(Yii::t('admin', 'Error'), 500);
        }

        /** @var ActiveRecord $model */
        $model = new $this->modelClass();
        $model->setIsNewRecord(true);
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = [$this->redirectUrl];
            if ($this->parameters) {
                $url['id'] = $model->id;
            }
            return $this->controller->redirect($url);
        } else {
            return $this->controller->render($this->view, [
                'model' => $model,
            ]);
        }
    }
}
