<?php

namespace app\modules\admin\controllers\actions\features;

use Yii;
use app\modules\admin\controllers\actions\CAction;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;

/**
 * Class CreateFeaturesAction
 * @package app\modules\admin\controllers\actions
 */
class CreateFeaturesAction extends CAction
{
    public $view = 'create';

    public function run()
    {
        if (!$this->modelClass) {
            throw new InvalidCallException(Yii::t('admin', 'Error'), 500);
        }

        /** @var ActiveRecord $model */
        $model = new $this->modelClass();
        $model->setIsNewRecord(true);
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->good_id = @Yii::$app->request->get()['GoodsHasFeatures']['good_id'];
            if ($model->save()) {
                $url = ['/admin/goods/view'];
                $url['id'] = $model->good_id;

                return $this->controller->redirect($url);
            }
        }

        return $this->controller->render($this->view, [
            'model' => $model,
        ]);
    }
}
