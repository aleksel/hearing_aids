<?php

namespace app\modules\admin\controllers\actions\features;

use Yii;
use app\modules\admin\controllers\actions\CAction;

/**
 * Class DeleteFeaturesAction
 * @package app\modules\admin\controllers\actions
 */
class DeleteFeaturesAction extends CAction
{
    public $redirectUrl = null;

    public function run($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->controller->redirect(['/admin/goods/view', 'id' => $model->good_id]);
    }
}