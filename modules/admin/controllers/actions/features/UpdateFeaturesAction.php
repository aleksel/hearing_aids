<?php

namespace app\modules\admin\controllers\actions\features;

use Yii;
use app\modules\admin\controllers\actions\CAction;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;

/**
 * Class UpdateFeaturesAction
 * @package app\modules\admin\controllers\actions
 */
class UpdateFeaturesAction extends CAction
{
    public $view = 'update';
    public $redirectUrl = null;
    public $parameters = true;

    public function run($id)
    {
        if (!$this->modelClass || !$this->redirectUrl) {
            throw new InvalidCallException(Yii::t('admin', 'Error'), 500);
        }

        /** @var ActiveRecord $model */
        $model = $this->findModel($id);
        $model->setIsNewRecord(false);
        //$model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = ['/admin/goods/view'];
            $url['id'] = $model->good_id;
            return $this->controller->redirect($url);
        } else {
            return $this->controller->render($this->view, [
                'model' => $model,
            ]);
        }
    }
}
