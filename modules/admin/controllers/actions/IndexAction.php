<?php

namespace app\modules\admin\controllers\actions;

use Yii;
use yii\base\InvalidCallException;

/**
 * Class IndexAction
 * @package app\modules\admin\controllers\actions
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
class IndexAction extends CAction
{
    public $view = 'index';
    public $modelSearchClass = null;

    public function run()
    {
        if (!$this->modelSearchClass) {
            throw new InvalidCallException(Yii::t('admin', 'Error'), 500);
        }

        $searchModel = new $this->modelSearchClass();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->controller->render($this->view, [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
