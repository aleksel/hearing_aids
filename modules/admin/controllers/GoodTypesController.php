<?php

namespace app\modules\admin\controllers;

use Yii;
use dosamigos\grid\ToggleAction;
use app\models\GoodTypesSearch;

/**
 * GoodTypesController implements the CRUD actions for GoodTypes model.
 */
class GoodTypesController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\GoodTypes';
        $modelSearchClass = 'app\models\GoodTypesSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'create' => [
                'class'       => 'app\modules\admin\controllers\actions\CreateAction',
                'view'        => 'create',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/good-types/view'
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\UpdateAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/good-types/view'
            ],
            'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/good-types'
            ],
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => $modelClass,
                'onValue' => '1',
                'offValue' => '0'
            ],
        ];

        return $actions + parent::actions();
    }
}
