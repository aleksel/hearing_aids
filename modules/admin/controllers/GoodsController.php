<?php

namespace app\modules\admin\controllers;

use dosamigos\grid\ToggleAction;

/**
 * GoodsController implements the CRUD actions for Goods model.
 */
class GoodsController extends AdminController
{
    public function actions()
    {
        $modelClass = 'app\models\Goods';
        $modelSearchClass = 'app\models\GoodsSearch';
        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'create' => [
                'class'       => 'app\modules\admin\controllers\actions\goods\CreateUpdateGoodsAction',
                'view'        => 'create',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/goods/view'
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\goods\CreateUpdateGoodsAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/goods/view'
            ],
            'view'   => [
                'class'            => 'app\modules\admin\controllers\actions\goods\ViewWithRelationsAction',
                'view'             => 'view',
                'modelClass'       => $modelClass,
                'modelSearchClass' => 'app\models\GoodsHasFeaturesSearch'
            ],
            'delete' => [
                'class'       => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/goods'
            ],
            'toggle' => [
                'class'      => ToggleAction::className(),
                'modelClass' => $modelClass,
                'onValue'    => '1',
                'offValue'   => '0'
            ],
        ];

        return $actions + parent::actions();
    }
}
