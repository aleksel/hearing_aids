<?php
namespace app\modules\admin\controllers;

use Yii;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;


/**
 * User controller
 */
class UserController extends AdminController
{
    public $defaultAction = 'login';

    public function actions()
    {
        $modelClass = 'app\models\User';
        $modelSearchClass = 'app\models\UserSearch';

        $actions = [
            'index'  => [
                'class'            => 'app\modules\admin\controllers\actions\IndexAction',
                'view'             => 'index',
                'modelSearchClass' => $modelSearchClass
            ],
            'create' => [
                'class'       => 'app\modules\admin\controllers\actions\CreateAction',
                'view'        => 'create',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/user/view'
            ],
            'update' => [
                'class'       => 'app\modules\admin\controllers\actions\UpdateAction',
                'view'        => 'update',
                'modelClass'  => $modelClass,
                'redirectUrl' => '/admin/user/view'
            ],
            'view'   => [
                'class'      => 'app\modules\admin\controllers\actions\ViewAction',
                'view'       => 'view',
                'modelClass' => $modelClass
            ],
            'delete' => [
                'class'      => 'app\modules\admin\controllers\actions\DeleteAction',
                'modelClass' => $modelClass,
                'redirectUrl' => '/admin/user'
            ],
        ];

        return $actions + parent::actions();
    }

    /**
     * Set the form.php as layout
     */
    private function setFormLayout()
    {
        $this->layout = 'form';
    }

    public function actionLogin()
    {
        $this->setFormLayout();

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {


            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout();
        \Yii::$app->setHomeUrl('/');
        return $this->goHome();
    }

    public function actionSignup()
    {
        $this->setFormLayout();

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $this->setFormLayout();

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('admin', 'Check your email for further instructions.'));

                //return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('admin', 'Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        $this->setFormLayout();

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('admin', 'New password was saved.'));

            //return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
