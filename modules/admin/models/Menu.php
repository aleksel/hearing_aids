<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * Menu is the model behind the frontend menu.
 */
class Menu extends Model
{
    protected $menu;

    public function init()
    {
        try {
            $this->menu = file_get_contents(Yii::getAlias('@app/views/layouts/_navigation.php'));
        } catch (Exception $e) {
            throw new Exception('Ошибка чтения файла с меню: ' . $e->getMessage());
        }
        parent::init();
    }

    /**
     * @return bool
     */
    public function save()
    {
        try {
            $result = file_put_contents(Yii::getAlias('@app/views/layouts/_navigation.php'), $this->menu);
        } catch (Exception $e) {
            $this->addError('menu', 'Ошибка сохранения меню: ' . $e->getMessage());
            return false;
        }
        return $result ? true : false;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['menu'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'menu' => Yii::t('admin', 'Menu'),
        ];
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param mixed $menu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    }
}
