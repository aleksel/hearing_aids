<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "banners".
 *
 * @property integer       $id
 * @property string        $name
 * @property string        $label
 * @property string        $is_showing
 *
 * @property BannerPages[] $bannerPages
 */
class Banners extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_showing'], 'in', 'range' => ['0', '1']],
            [['name', 'label'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'name'       => Yii::t('app', 'Name'),
            'label'      => Yii::t('app', 'Label'),
            'is_showing' => Yii::t('app', 'Is Showing'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerPages()
    {
        return $this->hasMany(BannerPages::className(), ['banner_id' => 'id'])->where([BannerPages::tableName() . '.is_showing' => '1']);
    }

    /**
     * Get banner pages array for carousel
     *
     * @param $bannerName
     * @return array
     */
    public static function getBanners($bannerName)
    {
        $bannerPages = [];
        $banner = self::find()
            ->joinWith('bannerPages', true)
            ->where([
                'name'                                   => $bannerName,
                self::tableName() . '.is_showing'     => '1'
            ])
            ->one();
        /** @var Banner $banner */
        if ($banner && $banner->bannerPages) {
            foreach ($banner->bannerPages as $item) {
                $bannerPages[] = [
                    'content' => $item->content,
                    'caption' => '',
                    'options' => [''],
                ];
            }
        }

        return $bannerPages;
    }
}
