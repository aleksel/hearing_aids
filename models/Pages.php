<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string  $title
 * @property string  $content
 * @property string  $url
 * @property string  $is_showing
 * @property string  $meta_title
 * @property string  $meta_description
 * @property string  $meta_keywords
 * @property integer $created_at
 * @property integer $updated_at
 */
class Pages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['created_at'], 'required', 'on' => ['create', 'default']],
            [['updated_at'], 'required', 'on' => ['default']],
            [['created_at', 'updated_at'], 'date', 'format' => 'php:' . Yii::$app->params['dateFormat']],
            [['content'], 'string'],
            [['is_showing'], 'in', 'range' => ['0', '1']],
            [['title', 'url', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            [['url'], 'unique']
        ];
    }

    public function afterFind()
    {
        if (Yii::$app->request->isAjax) {
            $this->created_at = date(Yii::$app->params['dateFormat'], $this->created_at ?: time());
            $this->updated_at = date(Yii::$app->params['dateFormat'], $this->updated_at ?: time());
        }

        parent::afterFind();
    }


    public function beforeValidate()
    {
        if (!($this instanceof PagesSearch)) {
            if ($this->scenario === 'create' && !$this->created_at) {
                $this->created_at = date(Yii::$app->params['dateFormat']);
            }
            if ($this->scenario !== 'create' && !$this->updated_at) {
                $this->updated_at = date(Yii::$app->params['dateFormat']);
            }
        }

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        if (!($this instanceof PagesSearch)) {
            if (is_string($this->created_at) && $date = strtotime($this->created_at)) {
                $this->created_at = $date;
            }
            if (is_string($this->updated_at) && $date = strtotime($this->updated_at)) {
                $this->updated_at = $date;
            }
        }

        return parent::afterValidate();
    }

    public function beforeSave($insert)
    {
        $this->url = trim($this->url, '\/');
        if (!$this->url) {
            $this->url = null;
        }
        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('app', 'ID'),
            'title'            => Yii::t('app', 'Title'),
            'content'          => Yii::t('app', 'Content'),
            'url'              => Yii::t('app', 'Url'),
            'is_showing'       => Yii::t('app', 'Is Showing'),
            'meta_title'       => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keywords'    => Yii::t('app', 'Meta Keywords'),
            'created_at'       => Yii::t('app', 'Created At'),
            'updated_at'       => Yii::t('app', 'Updated At'),
        ];
    }
}
