<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "features".
 *
 * @property integer            $id
 * @property string             $name
 *
 * @property GoodsHasFeatures[] $goodsHasFeatures
 */
class Features extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'on' => ['create', 'default']],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsHasFeatures()
    {
        return $this->hasMany(GoodsHasFeatures::className(), ['feature_id' => 'id']);
    }
}
