<?php
namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string  $login
 * @property string  $nik
 * @property string  $username
 * @property string  $password_hash
 * @property string  $email
 * @property integer $role
 * @property string  $reset_key
 * @property string  $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $password        write-only password
 * @property integer $change_password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $loginUrl = ['/admin/user/login'];
    public $change_password;

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_SUSPEND = 4;
    const STATUS_DELETED = 8;
    public static $STATUS = [
        self::STATUS_ACTIVE     => 'Активен',
        self::STATUS_NOT_ACTIVE => 'Не активен',
        self::STATUS_SUSPEND    => 'Заблокирован',
        self::STATUS_DELETED    => 'Удален'
    ];

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    public static $ROLE = [
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_USER  => 'Пользователь',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function beforeValidate()
    {
        if (!($this instanceof UserSearch)) {
            if ($this->scenario === 'create' && !$this->created_at) {
                $this->created_at = date(Yii::$app->params['dateFormat']);
            }
            if ($this->scenario !== 'create' && !$this->updated_at) {
                $this->updated_at = date(Yii::$app->params['dateFormat']);
            }
            if ($this->scenario === 'create') {
                $this->generateAuthKey();
            }
        }

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        if (!($this instanceof UserSearch)) {
            if (is_string($this->created_at) && $date = strtotime($this->created_at)) {
                $this->created_at = $date;
            }
            if (is_string($this->updated_at) && $date = strtotime($this->updated_at)) {
                $this->updated_at = $date;
            }
        }

        return parent::afterValidate();
    }

    public function beforeSave($insert)
    {
        if ($this->scenario === "create" || $this->change_password) {
            $this->setPassword($this->password_hash);
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        //$scenarios['update'] = [['nik'], 'safe'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],*/
            //TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::$STATUS)],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => array_keys(self::$ROLE)],
            [['username', 'email', 'auth_key', 'created_at'], 'required'],
            [['password_hash'], 'required', 'on' => 'create'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'date', 'format' => 'php:' . Yii::$app->params['dateFormat']],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['nik', 'change_password'], 'safe'],
            [['nik', 'username', 'email', 'reset_key', 'auth_key'], 'string', 'max' => 255],
            [['password_hash', 'role'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('admin', 'ID'),
            'nik'             => Yii::t('admin', 'Name'),
            'username'        => Yii::t('admin', 'Username'),
            'password_hash'   => Yii::t('admin', 'Password Hash'),
            'change_password' => Yii::t('admin', 'Change password'),
            'email'           => Yii::t('admin', 'E-mail'),
            'role'            => Yii::t('admin', 'Role'),
            'reset_key'       => Yii::t('admin', 'Reset Key'),
            'auth_key'        => Yii::t('admin', 'Auth Key'),
            'status'          => Yii::t('admin', 'Status'),
            'created_at'      => Yii::t('admin', 'Created At'),
            'updated_at'      => Yii::t('admin', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'reset_key' => $token,
            'status'    => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->reset_key = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->reset_key = null;
    }
}
