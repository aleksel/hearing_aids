<?php

namespace app\models;

use app\components\exceptions\FileExistsException;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "goods".
 *
 * @property integer            $id
 * @property integer            $type
 * @property string             $label
 * @property string             $url
 * @property integer            $price
 * @property string             $description_short
 * @property string             $description_full
 * @property string             $comment
 * @property string             $meta_title
 * @property string             $meta_description
 * @property string             $meta_keywords
 * @property string             $is_showing
 * @property string             $is_showing_popular
 *
 * @property GoodTypes          $type0
 * @property GoodsHasFeatures[] $goodsHasFeatures
 * @property GoodsHasFiles[]    $goodsHasFiles
 * @property Files[]            $files
 * @property GoodsHasGoods[]    $goodsHasGoods
 */
class Goods extends ActiveRecord
{
    const EVENT_FIND = 'event_find';

    public $change_image;
    public $features;
    public $change_image_small;
    public $change_instruction;
    public $image;
    public $image_gallery;
    public $image_small;
    public $image_small_gallery;
    public $instruction;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'price'], 'integer'],
            //[['image', 'image_small', 'instruction'], 'file'],
            [['label'], 'required', 'on' => ['create', 'default']],
            [['description_short', 'description_full', 'comment', 'is_showing', 'is_showing_popular'], 'string'],
            [['label', 'meta_title', 'meta_description', 'meta_keywords', 'url'], 'string', 'max' => 255],
            [['change_image', 'change_image_small', 'change_instruction'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('app', 'ID'),
            'type'               => Yii::t('app', 'Type'),
            'label'              => Yii::t('app', 'Label'),
            'url'                => Yii::t('app', 'Url'),
            'price'              => Yii::t('app', 'Price'),
            'description_short'  => Yii::t('app', 'Description Short'),
            'description_full'   => Yii::t('app', 'Description Full'),
            'comment'            => Yii::t('app', 'Comment'),
            'image_small'        => Yii::t('app', 'Image Small'),
            'image'              => Yii::t('app', 'Image'),
            'instruction'        => Yii::t('app', 'Instruction'),
            'meta_title'         => Yii::t('app', 'Meta Title'),
            'meta_description'   => Yii::t('app', 'Meta Description'),
            'meta_keywords'      => Yii::t('app', 'Meta Keywords'),
            'change_image'       => Yii::t('app', 'Change Image'),
            'change_image_small' => Yii::t('app', 'Change Image Small'),
            'change_instruction' => Yii::t('app', 'Change Instruction'),
            'is_showing'         => Yii::t('app', 'Is Showing'),
            'is_showing_popular' => Yii::t('app', 'Is Showing Popular'),
        ];
    }

    public function afterDelete()
    {
        if ($this->files) {
            foreach ($this->files as $file) {
                @unlink($file->path);
                $file->delete();
            }
        }
        parent::afterDelete();
    }


    public function afterFind()
    {
        if ($this->files) {
            /** @var Files $file */
            foreach ($this->files as $file) {
                if ($file->type === Files::TYPE_INSTRUCTION_GOOD) {
                    $this->instruction = $file->url;
                } elseif ($file->type === Files::TYPE_IMAGE_GOOD) {
                    $this->image[] = $file;
                } elseif ($file->type === Files::TYPE_IMAGE_SMALL_GOOD) {
                    $this->image_small[] = $file;
                }
            }
        }

        if ($this->goodsHasFeatures) {
            foreach ($this->goodsHasFeatures as $feature) {
                if (@$feature->feature->name && $feature->is_showing) {
                    $this->features[$feature->feature->id] = [
                        'name'  => $feature->feature->name,
                        'value' => $feature->value
                    ];
                }
            }
        }

        $this->on(self::EVENT_FIND, [$this, 'afterFindFiles']);
        parent::afterFind();
    }

    /**
     * Создаем массивы для галлереи карточки товара в админке
     */
    public function afterFindFiles()
    {
        if ($this->files) {
            /** @var Files $file */
            foreach ($this->files as $file) {
                if ($file->type === Files::TYPE_IMAGE_GOOD) {
                    $this->image_gallery[] = [
                        'url'     => $file->url,
                        'src'     => $file->url,
                        'options' => [
                            'title' => $this->label . '-' . $file->name
                        ]
                    ];
                } elseif ($file->type === Files::TYPE_IMAGE_SMALL_GOOD) {
                    $this->image_small_gallery[] = [
                        'url'     => $file->url,
                        'src'     => $file->url,
                        'options' => [
                            'title' => $this->label . '-' . $file->name
                        ]
                    ];
                }
            }
        }
    }

    /**
     * Создаем массив для формы редактирования товара в админке
     *
     * @param string $type Тип файла
     * @return array
     */
    public function getFileForForm($type)
    {
        $result = [];
        if ($this->files) {
            /** @var Files $file */
            foreach ($this->files as $file) {
                if ($file->type === $type && $type === Files::TYPE_INSTRUCTION_GOOD) {
                    $result[] = Html::a(
                        Html::img(
                            '/images/pdf.jpg',
                            [
                                'class' => 'file-preview-image',
                                'alt'   => $this->label . '-' . $file->name,
                                'title' => $this->label . '-' . $file->name,
                                'style' => 'width: 43px; height: 54px;'
                            ]
                        ),
                        $file->url,
                        ['target' => '_blank']
                    );
                } elseif ($file->type === $type) {
                    $result[] = Html::img(
                        $file->url,
                        [
                            'class' => 'file-preview-image',
                            'alt'   => $this->label . '-' . $file->name,
                            'title' => $this->label . '-' . $file->name
                        ]
                    );
                }
            }
        }

        return $result;
    }

    /**
     * Удаляем старые файлы
     *
     * @param                $fileType
     * @param UploadedFile[] $images
     */
    private function deleteOldFiles($fileType, $images)
    {
        // Массив с именами полученых файлов
        $images = ArrayHelper::getColumn($images, 'name');

        // Удаляем старые файлы
        if ($this->scenario !== "create" && $this->files) {
            foreach ($this->files as $file) {
                if ($file->type === $fileType && !in_array($file->name, $images)) {
                    @unlink($file->path);
                    $file->delete();
                } elseif (in_array($file->name, $images)) {
                    $file->delete();
                }
            }
        }
    }

    /**
     * Сохраняем файлы
     *
     * @param Goods  $model
     * @param string $path
     * @param string $changeField
     * @param string $field
     * @param string $fileType
     * @throws Exception
     * @throws FileExistsException
     * @throws \Exception
     */
    public function saveFiles($model, $path, $changeField, $field, $fileType)
    {
        if ($model->scenario === "create" || $model->$changeField) {

            // Если получены файлы
            if ($images = UploadedFile::getInstances($model, $field)) {
                foreach ($images as $image) {
                    // Флаг что существующий файл относится к текущей записи
                    $currentFile = false;
                    if ($model->files) {
                        foreach ($model->files as $file) {
                            if ($file->type === $fileType && $file->name === $image->name) {
                                $currentFile = true;
                                break;
                            }
                        }
                    }
                    $filePath = $path . $field . '/' . $image->name;
                    if (is_file($filePath) && !$currentFile) {
                        throw new FileExistsException('Файл с заданым именем уже существует, путь: ' . $filePath, 500, null, $field);
                    }

                    $filesModel = new Files();
                    $filesModel->name = $image->name;
                    $filesModel->type = $fileType;
                    $filesModel->url = Yii::$app->params['goodsFilePath'] . $field . '/' . $image->name;
                    $filesModel->path = $filePath;
                    $filesModel->extension = $image->extension;

                    if (!$filesModel->save()) {
                        throw new Exception('Ошибка сохранения записи в таблицу files', 500);
                    }

                    $goodsHasFilesModel = new GoodsHasFiles();
                    $goodsHasFilesModel->good_id = $model->primaryKey;
                    $goodsHasFilesModel->file_id = $filesModel->primaryKey;

                    if (!$goodsHasFilesModel->save()) {
                        throw new Exception('Ошибка сохранения записи в таблицу goods_has_files', 500);
                    }

                    if (!$image->saveAs($filePath)) {
                        throw new Exception('Ошибка сохранения файла на диск', 500);
                    }
                }
            }

            $this->deleteOldFiles($fileType, $images);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(GoodTypes::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsHasFeatures()
    {
        return $this->hasMany(GoodsHasFeatures::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsHasFiles()
    {
        return $this->hasMany(GoodsHasFiles::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(Files::className(), ['id' => 'file_id'])->viaTable('goods_has_files', ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsF()
    {
        return $this->hasMany(GoodsHasGoods::className(), ['good_id_f' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsS()
    {
        return $this->hasMany(GoodsHasGoods::className(), ['good_id_s' => 'id']);
    }
}
