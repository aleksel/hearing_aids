<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "callbacks".
 *
 * @property integer $id
 * @property string  $phone
 * @property string  $email
 * @property string  $message
 * @property string  $comment
 * @property integer $created_at
 * @property integer $updated_at
 */
class Callbacks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'callbacks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['phone', 'email'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['message', 'comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'phone'      => Yii::t('app', 'Phone'),
            'email'      => Yii::t('app', 'Email'),
            'message'    => Yii::t('app', 'Message'),
            'comment'    => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
