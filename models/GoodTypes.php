<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "good_types".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $is_showing
 *
 * @property Goods[] $goods
 */
class GoodTypes extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_showing'], 'string', 'on' => ['create', 'default']],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'name'       => Yii::t('app', 'Name'),
            'is_showing' => Yii::t('app', 'Is Showing'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::className(), ['type' => 'id']);
    }
}
