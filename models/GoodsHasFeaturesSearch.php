<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GoodsHasFeatures;

/**
 * GoodsHasFeaturesSearch represents the model behind the search form about `app\models\GoodsHasFeatures`.
 */
class GoodsHasFeaturesSearch extends GoodsHasFeatures
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'good_id', 'feature_id'], 'integer'],
            [['value', 'is_showing'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GoodsHasFeatures::find()->joinWith('good', true);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'goods.label' => $this->good_id,
            'feature_id' => $this->feature_id,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'goods.label', $this->good_id])
            ->andFilterWhere(['like', 'is_showing', $this->is_showing]);

        return $dataProvider;
    }
}
