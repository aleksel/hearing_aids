<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $value
 * @property string  $label
 * @property string  $type
 * @property integer $edit
 */
class Settings extends ActiveRecord
{
    /**
     * @var array Setting name = Setting value
     */
    public static $settings = [];

    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_CHECKBOX = 'checkbox';
    public static $TYPE = [
        self::TYPE_STRING   => 'Строка',
        self::TYPE_TEXT     => 'Текст',
        self::TYPE_CHECKBOX => 'Чекбокс'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value'], 'string'],
            [['edit'], 'integer'],
            [['name', 'label'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 100],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'    => Yii::t('app', 'ID'),
            'name'  => Yii::t('app', 'Системное название'),
            'value' => Yii::t('app', 'Зачение'),
            'label' => Yii::t('app', 'Название'),
            'type'  => Yii::t('app', 'Тип'),
            'edit'  => Yii::t('app', 'Изменяемое'),
        ];
    }

    public static function getSettings()
    {
        if (!self::$settings) {
            $settings = Settings::find()->all();

            foreach ($settings as $setting) {
                self::$settings[$setting->name] = $setting->value;
            }
        }

        return self::$settings;
    }
}
