<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GoodsHasGoods;

/**
 * GoodsHasGoodsSearch represents the model behind the search form about `app\models\GoodsHasGoods`.
 */
class GoodsHasGoodsSearch extends GoodsHasGoods
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id_f', 'good_id_s'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GoodsHasGoods::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'good_id_f' => $this->good_id_f,
            'good_id_s' => $this->good_id_s,
        ]);

        return $dataProvider;
    }
}
