<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "banner_pages".
 *
 * @property integer $id
 * @property string  $content
 * @property string  $is_showing
 * @property integer $order
 * @property integer $banner_id
 *
 * @property Banners $banner
 */
class BannerPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'order'], 'required'],
            [['content'], 'string', 'on' => ['create', 'default']],
            [['is_showing'], 'in', 'range' => ['0', '1']],
            [['order', 'banner_id'], 'integer'],
            [['banner_id'], 'in', 'range' => ArrayHelper::getColumn(Banners::find()->select('id')->asArray()->all(), 'id')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'content'    => Yii::t('app', 'Content'),
            'is_showing' => Yii::t('app', 'Is Showing'),
            'order'      => Yii::t('app', 'Order'),
            'banner_id'  => Yii::t('app', 'Banner ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banners::className(), ['id' => 'banner_id']);
    }
}
