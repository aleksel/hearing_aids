<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Goods;

/**
 * GoodsSearch represents the model behind the search form about `app\models\Goods`.
 */
class GoodsSearch extends Goods
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'price', 'image_small', 'instruction'], 'integer'],
            [[
                'label',
                'description_short',
                'description_full',
                'comment',
                'image',
                'is_showing',
                'is_showing_popular',
                'meta_title',
                'meta_description',
                'meta_keywords',
                'url'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Goods::find()->with(['files']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'    => $this->id,
            'type'  => $this->type,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'description_short', $this->description_short])
            ->andFilterWhere(['like', 'description_full', $this->description_full])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'meta_title', $this->comment])
            ->andFilterWhere(['like', 'meta_description', $this->comment])
            ->andFilterWhere(['like', 'meta_keywords', $this->comment])
            ->andFilterWhere(['like', 'is_showing', $this->is_showing])
            ->andFilterWhere(['like', 'is_showing_popular', $this->is_showing_popular]);

        return $dataProvider;
    }
}
