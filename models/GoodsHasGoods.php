<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods_has_goods".
 *
 * @property integer $good_id_f
 * @property integer $good_id_s
 *
 * @property Goods $goodIdF
 * @property Goods $goodIdS
 */
class GoodsHasGoods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_has_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id_f', 'good_id_s'], 'required'],
            [['good_id_f', 'good_id_s'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'good_id_f' => Yii::t('app', 'Good Id F'),
            'good_id_s' => Yii::t('app', 'Good Id S'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsF()
    {
        return $this->hasOne(Goods::className(), ['id' => 'good_id_f']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsS()
    {
        return $this->hasOne(Goods::className(), ['id' => 'good_id_s']);
    }
}
