<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "goods_has_features".
 *
 * @property integer  $id
 * @property integer  $good_id
 * @property integer  $feature_id
 * @property string   $value
 * @property string   $is_showing
 *
 * @property Features $feature
 * @property Goods    $good
 */
class GoodsHasFeatures extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_has_features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id', 'feature_id'], 'required'],
            [['good_id', 'feature_id'], 'integer'],
            [['is_showing'], 'string', 'on' => ['create', 'default']],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'good_id'    => Yii::t('app', 'Good ID'),
            'feature_id' => Yii::t('app', 'Feature ID'),
            'value'      => Yii::t('app', 'Value'),
            'is_showing' => Yii::t('app', 'Is Showing'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this->hasOne(Features::className(), ['id' => 'feature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Goods::className(), ['id' => 'good_id']);
    }
}
