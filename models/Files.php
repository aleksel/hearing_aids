<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "files".
 *
 * @property integer         $id
 * @property string          $name
 * @property string          $type
 * @property string          $path
 * @property string          $url
 * @property string          $extension
 * @property integer         $created_at
 * @property integer         $updated_at
 *
 * @property GoodsHasFiles[] $goodsHasFiles
 * @property Goods[]         $goods
 * @property mixed           TYPES
 */
class Files extends ActiveRecord
{
    const TYPE_INSTRUCTION_GOOD = 'instruction_good';
    const TYPE_IMAGE_SMALL_GOOD = 'image_small_good';
    const TYPE_IMAGE_GOOD = 'image_good';
    public static $TYPES = [
        self::TYPE_IMAGE_GOOD, self::TYPE_IMAGE_SMALL_GOOD, self::TYPE_INSTRUCTION_GOOD
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],*/
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'path'], 'required'],
            [['type'], 'string'],
            [['type'], 'in', 'range' => self::$TYPES],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'path', 'url'], 'string', 'max' => 255],
            [['extension'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'name'       => Yii::t('app', 'Name'),
            'type'       => Yii::t('app', 'Type'),
            'path'       => Yii::t('app', 'Path'),
            'url'        => Yii::t('app', 'Url'),
            'extension'  => Yii::t('app', 'Extension'),
            'created_at' => Yii::t('app', 'Created At'),
            'updatet_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsHasFiles()
    {
        return $this->hasMany(GoodsHasFiles::className(), ['file_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Goods::className(), ['id' => 'good_id'])->viaTable('goods_has_files', ['file_id' => 'id']);
    }
}
