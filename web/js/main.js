(function () {
    "use strict";

    var app = {
        initialize: function () {
            this.menuHover();
            this.setUpListeners();
        },

        modules: function () {

        },

        showScrollUp: function () {
            if ($(this).scrollTop() > 200) {
                $('header .scroll-top button').fadeIn(200);
            } else {
                $('header .scroll-top button').fadeOut(200);
            }
        },

        scrollUp: function () {
            $("body, html").animate({scrollTop: 0}, 500);
            return false;
        },

        setUpListeners: function () {
            $(window).scroll(this.showScrollUp);
            $(document).on('click', 'header .scroll-top button', $.proxy(this.scrollUp, this));
        },

        menuHover: function () {
            $('.nav').children('.dropdown').hover(
                function () {
                    $(this).addClass('open');
                },
                function () {
                    $(this).removeClass('open');
                }
            )
        }
    }
    app.initialize();
}());