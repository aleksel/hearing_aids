<?php

use app\components\helpers\StaticPagesHelper;
use app\models\Settings;

return function ($event) {
    // \Yii::$app->request->getUrl();
    if ($dateFormat = app\models\Settings::find()->select('value')->where(['name' => 'dateFormat'])->one()->value) {
        \Yii::$app->params['dateFormat'] = $dateFormat;
    }

    \Yii::$app->params['settings'] = Settings::getSettings();
    StaticPagesHelper::getInstance()->run();
};
