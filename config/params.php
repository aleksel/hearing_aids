<?php

return [
    'adminEmail'                    => 'undefined.mail@yandex.ru',
    'supportEmail'                  => 'undefined.mail@yandex.ru',
    'adminHomeUrl'                  => '/admin/user/index',
    'user.passwordResetTokenExpire' => 3600,
    'dateFormat'                    => 'd.m.Y H:i',
    'goodsFilePath'                 => '/files/goods/',
    'settings'                      => [] // Получаем из базы
];
