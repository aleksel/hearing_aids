<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id'               => 'Hearing aids',
    'name'             => 'Hearing aids',
    'basePath'         => dirname(__DIR__),
    'bootstrap'        => ['log'],
    'language'         => 'ru-RU',
    'defaultRoute'     => 'main/index',
    'modules'          => [
        'admin' => [
            'class' => 'app\modules\admin\admin',
        ],
    ],
    'on beforeRequest' => require(__DIR__ . '/initApp.php'),
    'components'       => [
        'assetManager' => [
            'bundles' => [
                /*'yii\web\YiiAsset' => [
                    'js' => [YII_ENV_DEV ? 'yii.js' : 'yii.min.js']
                ],*/
                'yii\web\JqueryAsset'                => [
                    'js' => [YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js']
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js']
                ],
                'yii\bootstrap\BootstrapAsset'       => [
                    'css' => [YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css']
                ],
                /*'dosamigos\gallery\GalleryAsset' => [
                    'js' => ['dosamigos-blueimp-gallery.js', 'vendor/js/blueimp-gallery.js']
                ]*/
            ],
        ],
        'i18n'         => [
            'translations' => [
                '*' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap'        => [
                        'main'  => 'main.php',
                        'admin' => 'admin.php',
                    ],
                ],
            ],
        ],
        'request'      => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'XUQ-un8wUPQ7LzfbojdC6v4gR8FEmbx8',
        ],
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'user'         => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
            /*Yii::$app->mailer->compose('layouts/html', ['content' => '<h1>test</h1>'])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo('andrej.savchuk@gmail.com')
                ->setSubject("tressdg")
                ->send();*/
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.yandex.ru',
                'username'   => 'undefined.mail',
                'password'   => '2MtxB7qt',
                'port'       => '587',
                'encryption' => 'tls',
            ],
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                // your rules go here
                'admin'           => 'admin/user/login',
                'catalog/<title>' => 'catalog/item'
            ],
        ],
        'db'           => require(__DIR__ . (YII_ENV_DEV ? '/db_dev.php' : '/db_prod.php')),
    ],
    'params'           => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.56.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.56.*'],
    ];
}

return $config;
