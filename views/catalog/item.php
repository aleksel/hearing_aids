<?php

use app\components\helpers\FeaturesHelper;
use app\models\Banners;
use yii\bootstrap\Carousel;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $item \app\models\Goods */

if (@$item->meta_title) {
    $this->title = $item->meta_title;
}
if (@$item->meta_description) {
    $this->registerMetaTag(['name' => 'description', 'content' => $item->meta_description]);
}
if (@$item->meta_keywords) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => $item->meta_keywords]);
}
?>

<div class="item-container">
    <?php
    $this->params['breadcrumbs'][] = ['label' => 'каталог слуховых аппаратов', 'url' => ['/catalog']];
    $this->params['breadcrumbs'][] = strtoupper($item->label);
    ?>
    <?= Breadcrumbs::widget([
        'homeLink' => ['label' => 'главная страница', 'url' => ['/']],
        'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <div class="row">
        <div class="col-md-5">
            <?php
            if (@$item->image[0] && is_file($path . 'image/' . $item->image[0]->url)) {
                ?>
                <div class="item-image" style="background: url('<?= $item->image[0]->url; ?>') no-repeat 50% 50%"></div>
            <?php
            } else {
                ?>
                <div class="item-image" style="background: url('/images/blank.png') no-repeat 50% 50%"></div>
            <?php
            }
            ?>
            <?php
            if (@$item->instruction) {
                ?>
                <div class="item-documentation">
                    документация:
                    <br/>
                    <br/>
                    <div class="row">
                        <div class="item-instruction-icon col-md-2">
                            <img class="item-instruction-image" src="/images/pdf.jpg" alt="" style="width: 43px; height: 54px;" />
                        </div>
                        <div class="item-instruction-button col-md-8">
                            <a href="<?= $item->instruction; ?>" style="text-decoration: none">
                                <button class="button-css font_main">скачать инструкцию</button>
                            </a>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
        <div class="col-md-6">
            <div class="item-label font_main"><?= $item->label; ?></div>
            <div class="item-description font_main"><?= $item->description_full; ?></div>
            <br/>
            <div class="item-price font_main"><?= $item->price; ?> рублей</div>
            <br/>
            <br/>
            <br/>
            <div class="item-features font_main">
                <div class="item-features-title">Технические характеристики:</div>
                <div class="item-features-all">
                    <?= FeaturesHelper::getFeatures(@$item->features, [2, 1, 3, 4, 5]); ?>
                </div>
                <div class="item-features-button">
                    <button class="button-css font_main">купить слуховой аппарат</button>
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <?php
        if ($withGoods) {
            ?>
            <div class="goods-with font_main">Сопутствующие товары</div>
            <div class="item-good-type"></div>
            <div class="item-with-carousel">
                <?= Carousel::widget([
                    'items'   => $withGoods,
                    'options' => [
                        'class'         => 'carousel slide',
                        'data-interval' => @Yii::$app->params['settings']['bannerIntervalOnItemWith'] ?: 3000
                    ]
                ]);
                ?>
            </div>
        <?php
        }
        ?>

        <?php
        if ($similarGoods) {
            ?>
            <div class="goods-with font_main">Похожие товары</div>
            <div class="item-good-type"></div>
            <div class="item-with-carousel">
                <?= Carousel::widget([
                    'items'   => $similarGoods,
                    'options' => [
                        'class'         => 'carousel slide',
                        'data-interval' => @Yii::$app->params['settings']['bannerIntervalOnItemSimilar'] ?: 3000
                    ]
                ]);
                ?>
            </div>
        <?php
        }
        ?>
        <br/>
    </div>
</div>
        <div style="clear: both"></div>
