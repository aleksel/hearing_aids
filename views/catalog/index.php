<?php
/* @var $this yii\web\View */

use app\components\helpers\FeaturesHelper;

if (@Yii::$app->params['settings']['catalogPageTitle']) {
    $this->title = Yii::$app->params['settings']['catalogPageTitle'];
}
if (@Yii::$app->params['settings']['catalogPageDescription']) {
    $this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['settings']['catalogPageDescription']]);
}
if (@Yii::$app->params['settings']['catalogPageKeywords']) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['settings']['catalogPageKeywords']]);
}

$lastCategory = '';
?>
<div class="catalog-container">
    <h1 class="font_main">Наши слуховые аппараты</h1>
    <div class="font_main catalog-sort-title">
        сортировка каталога:
    </div>
    <div class="hedge"></div>
    <button class="button-sort active">Все аппараты</button>
    <button class="button-sort">Эконом-класс</button>
    <button class="button-sort">Средний-класс</button>
    <button class="button-sort">Сверхмощные</button>
    <button class="button-sort">Полезные вещи</button>
    <br/>
    <br/>
    <br/>
    <div class="catalog-item-wrap" autofocus>
        <?php
        foreach ($goods as $item) {
            if ($item->type !== $lastCategory) {
                switch ($item->type) {
                    case (1):
                        echo '<button class="button-sort active catalog-item-type-' . $item->type . '">Эконом-класс</button>';
                        break;
                    case (3):
                        echo '<button class="button-sort active catalog-item-type-' . $item->type . '">Средний-класс</button>';
                        break;
                    case (5):
                        echo '<button class="button-sort active catalog-item-type-' . $item->type . '">Сверхмощные</button>';
                        break;
                }
                echo '<div class="catalog-item-good-type"></div>';
                $lastCategory = $item->type;
            }
            ?>

            <div class="row">
                <div class="catalog-item-divider"></div>
                <div class="col-md-3 catalog-item-image" style="background: url('<?= $item->image_small[0]->url; ?>') no-repeat 50% 50%"></div>
                <div class="col-md-3 catalog-item-title-wrap">
                    <div class="catalog-item-title">
                        <div class="font_main item-title">
                            <?= $item->label; ?>
                        </div>
                        <div class="font_main item-price">
                            <?= $item->price; ?> рублей
                        </div>
                        <div class="font_main item-channel-count">
                            <?= @$item->features[6]['value']; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 catalog-item-features-wrap">
                    <div class="catalog-item-features">
                        <?= FeaturesHelper::getFeatures(@$item->features, [2, 1, 3, 4, 5]); ?>
                    </div>
                </div>
                <div class="col-md-3 catalog-item-button-wrap">
                    <div class="catalog-item-button">
                        <?php
                        if ($item->url) {
                            ?>
                            <a style="text-decoration: none" href="/catalog/<?= $item->url; ?>">
                                <button class="button-css font_main">смотреть подробнее</button>
                            </a>
                        <?php
                        }
                        ?>
                    </div>
                </div>

            </div>

            <?php
        }
        ?>
    </div>
</div>