<?php
/* @var $this yii\web\View */
use app\models\Banners;
use yii\bootstrap\Carousel;

if (@Yii::$app->params['settings']['mainPageTitle']) {
    $this->title = Yii::$app->params['settings']['mainPageTitle'];
}
if (@Yii::$app->params['settings']['mainPageDescription']) {
    $this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->params['settings']['mainPageDescription']]);
}
if (@Yii::$app->params['settings']['mainPageKeywords']) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->params['settings']['mainPageKeywords']]);
}
?>
<?php
$bannerPages = array_merge(Banners::getBanners('main_banner'), $popularGoods);
if ($bannerPages) {
    ?>
    <div class="main-carousel">
        <?= Carousel::widget([
            'items'   => $bannerPages,
            // the item contains only the image
            //'<img src="http://twitter.github.io/bootstrap/assets/img/bootstrap-mdo-sfmoma-01.jpg"/>',
            // equivalent to the above
            //['content' => '<img src="http://twitter.github.io/bootstrap/assets/img/bootstrap-mdo-sfmoma-02.jpg"/>'],
            // the item contains both the image and the caption
            'options' => [
                'class'         => 'carousel slide',
                'data-interval' => @Yii::$app->params['settings']['bannerIntervalOnMain'] ?: 3000
            ]
        ]);
        ?>
    </div>
    <?php
}
?>
<div class="advantages">
    <div class="hedge"></div>
    <div class="advantages-content">
        <h3 class="font_main">современный слуховой аппарат это:</h3>
        <div class="advantages-line font_main">
            <div class="advantages-item">
                <span style="background : url('/images/icon_1.png');"></span>
                <span> 100% цифровая обработка звука</span>
            </div>
            <div class="advantages-item">
                <span style="background : url('/images/icon_2.png');"></span>
                <span> Точная индивидуальная компьютерная настройка</span>
            </div>
            <div class="advantages-item">
                <span style="background : url('/images/icon_3.png');"></span>
                <span> Простота, удобство и безопасность в использовании</span>
            </div>
        </div>
        <div class="advantages-line font_main">
            <div class="advantages-item">
                <span style="background : url('/images/icon_4.png');"></span>
                <span> Комфорт и непревзойденная разборчивость речи</span>
            </div>
            <div class="advantages-item">
                <span style="background : url('/images/icon_5.png');"></span>
                <span> Передовые технологии и современный дизайн</span>
            </div>
            <div class="advantages-item">
                <span style="background : url('/images/icon_6.png');"></span>
                <span> Гарантия качества</span>
            </div>
        </div>
    </div>
    <div class="hedge" style="clear: both;"></div>
</div>
<div class="awards-block">
    <div class="awards-block-content font_main">
        <p style="font-size: 25px; text-transform: uppercase; font-weight: bold">Информация о нас</p>
        <p style="font-size: 17px; margin-top: -17px">альфаслух</p>
        <p style="font-size: 25px; margin-top: 27px; font-weight: bold">Краткая история организации:</p>
        <p style="font-size: 14px; margin-top: -10px">
            Баадер родился в Мюнхене 27 марта 1765 года.
            Получил медицинское образование, изучал минералогию в Англии в 1792-1796 годах.
            Как и его друг Шеллинг, был членом кружка интеллектуалов в Мюнхене.
            Черпал вдохновение в мистических трудах Якоба Бёме и неоплатоников.
            Вышел в отставку в должности суперинтенданта угольных шахт в Баварии в 1820 году
            и посвятил себя философии и теологии. Профессор философии и теологии
            Мюнхенского университета в 1826-1838 годах. Основал журнал "Эос" ("Eos").
            Умер Баадер в Мюнхене 23 мая 1841 года.
        </p>
        <p style="font-size: 25px; margin-top: 50px; font-weight: bold">Награды и признания:</p>
        <p style="font-size: 14px; margin: -10px 0 25px 0">Описание достижений по пунктам.<br/>Призы и т.д.</p>
        <div class="awards-block-line">
            <span style="background : url('/images/award_1.jpg') no-repeat;"></span>
            <span style="background : url('/images/award_2.jpg') no-repeat;"></span>
            <span style="background : url('/images/award_3.jpg') no-repeat;"></span>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<div class="your-doctor">
    <div class="your-doctor-content font_main">
        <h3 style="font-size: 25px; text-transform: uppercase; font-weight: bold; padding-top: 25px;">
            Смирнов Владимир иванович
        </h3>
        <p style="color: #ffff00; font-size: 17px; margin: -12px 0 0 0">ваш доктор</p>
        <button class="button-css">бесплатная консультация</button>
        <img src="/images/your-doctor-photo.png" alt="Фото вашего доктора"/>
        <span class="your-doctor-info">
            <p style="font-size: 17px;">Врач высшей категории</p>
            <p style="font-size: 17px; margin-top: -12px">Опыт в работе более 8-ми лет.</p>
            <p style="font-size: 25px; margin-top: 27px; font-weight: bold">Краткая биография:</p>
            <p style="font-size: 14px; margin-top: -10px">
                Баадер родился в Мюнхене 27 марта 1765 года.
                Получил медицинское образование, изучал минералогию в Англии в 1792-1796 годах.
                Как и его друг Шеллинг, был членом кружка интеллектуалов в Мюнхене.
                Черпал вдохновение в мистических трудах Якоба Бёме и неоплатоников.
                Вышел в отставку в должности суперинтенданта угольных шахт в Баварии в 1820 году
                и посвятил себя философии и теологии. Профессор философии и теологии
                Мюнхенского университета в 1826-1838 годах. Основал журнал "Эос" ("Eos").
                Умер Баадер в Мюнхене 23 мая 1841 года.
            </p>
            <p id="map-contact" style="font-size: 25px; margin: 40px 0 0 444px; font-weight: bold">Награды и признания:</p>
            <p class="your-doctor-last-paragraph" style="font-size: 14px; margin: 0 0 0 444px">Описание достижений по пунктам.<br/>Призы и т.д.</p>
        </span>
    </div>
</div>