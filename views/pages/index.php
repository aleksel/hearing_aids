<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
if ($page->meta_title) {
    $this->title = $page->meta_title;
}
if ($page->meta_description) {
    $this->registerMetaTag(['name' => 'description', 'content' => $page->meta_description]);
}
if ($page->meta_keywords) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => $page->meta_keywords]);
}
?>
<div class="site-about" style="width: 960px; margin: auto;">
    <div>
        <?= $page->content; ?>
    </div>
</div>
