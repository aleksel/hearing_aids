<?php

use app\components\widgets\Nav;
use yii\bootstrap\NavBar;

/**
 * Top menu definition.
 *
 * @var $this \yii\web\View
 */
NavBar::begin([
    'brandLabel' => '<div class="header-logo"></div>' .
        '<span class="header-logo-title font_main">АльфаСлух</span>' .
        '<span class="header-logo-text font_main">слуховые аппараты из швейцарии</span>',
    'brandUrl'   => Yii::$app->homeUrl,
    'options'    => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items'   => [
        [
            'label' => 'Главная',
            'url'   => ['/']
        ],
        ['label' => 'О компании', 'url' => ['/about']],
        ['label' => 'Цены', 'url' => ['/catalog']],
        ['label' => 'Отзывы', 'url' => ['/reviews']]
    ]
]);
NavBar::end();
