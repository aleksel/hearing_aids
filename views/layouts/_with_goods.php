<?php
/**
 * @var $item \app\models\Goods
 */
?>
<div class="item-with-carousel-slide">
    <div class="row">
        <?php
        if ($item->url) {
            echo '<a href="/catalog/' . $item->url . '">';
        }
        ?>
        <div class="col-md-3">
            <img class="item-with-carousel-image" src="<?= @$item->image_small[0]->url; ?>" alt=""/>
        </div>
        <div class="item-with-carousel-description col-md-3">
            <div class="font_main item-with-carousel-title">
                <?= $item->label; ?>
            </div>
            <div class="font_main item-with-carousel-description-price">
                <?= $item->price; ?> рублей
            </div>
        </div>
        <?php
        if ($item->url) {
            echo '</a>';
        }
        ?>
        <?php
        if ($second) {
            ?>
            <?php
            if ($second->url) {
                echo '<a href="/catalog/' . $second->url . '">';
            }
            ?>
            <div class="col-md-3">
                <img class="item-with-carousel-image" src="<?= @$second->image_small[0]->url; ?>" alt=""
                     style="margin-left: 50px"/>
            </div>
            <div class="item-with-carousel-description col-md-3" style="margin-left: -40px;">
                <div class="font_main item-with-carousel-title">
                    <?= $second->label; ?>
                </div>
                <div class="font_main item-with-carousel-description-price">
                    <?= $second->price; ?> рублей
                </div>
            </div>
            <?php
            if ($second->url) {
                echo '</a>';
            }
            ?>
        <?php
        }
        ?>
    </div>
</div>
