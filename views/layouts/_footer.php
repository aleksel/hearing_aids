<footer class="footer">
    <div class="map-contact">
        <?= @\Yii::$app->params['settings']['footerMap']; ?>
    </div>
    <div class="contacts-info">
        <div class="contacts-info-background">
            <div class="contacts-info-content font_main">
                <p style="font-size: 25px; text-transform: uppercase; font-weight: bold; padding: 37px 0 22px 0;">
                    Наша контактная информация
                </p>
                <div style="float: left; width: 250px; height: 80px;">
                    <span class="footer-metro-marker"></span>
                    <a href="#map-contact"><?= @\Yii::$app->params['settings']['footerMetro']; ?></a>
                </div>
                <div style="float: left; width: 330px; height: 95px;">
                    <p style="margin-top: -15px;">Телефоны:</p>
                    <span class="footer-phone font_main">
                        <?= @\Yii::$app->params['settings']['footerPhone']; ?>
                    </span>

                </div>
                <p style="float: left; margin-top: -18px; width: 246px;" class="footer-phone font_main">
                    <?= @\Yii::$app->params['settings']['workTime']; ?>
                </p>
                <button style="margin-top: -10px; float: left" class="button-css font_main">заказать звонок</button>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>
    <div class="footer-banner"></div>
</footer>