<?php

use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */


?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
<?= $this->renderFile('@app/views/layouts/_header.php'); ?>
    <div class="wrap">
        <div class="container">
            <?= $content ?>
        </div>
    </div>
<?= $this->renderFile('@app/views/layouts/_footer.php'); ?>
<?php $this->endContent();