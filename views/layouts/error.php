<?php
/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
    <div class="wrap">
        <div class="container">
            <?= $content ?>
        </div>
    </div>
<?php $this->endContent();
