<?php
use app\models\Banners;
use yii\bootstrap\Carousel;

?>
<header class="">

    <div class="scroll-top">
        <button style="display: none" class="button-css font_main"><div>‹</div></button>
    </div>
    <div class="header navbar-fixed-top">
        <div class="header-wrap">
            <div class="header-overflow">
                <div class="header-contact-outer col-md-offset-2">
                    <div class="header-contact">
                        <span class="header-metro-marker"></span>
                        <span class="header-metro font_main">
                            <a href="#map-contact"><?= @\Yii::$app->params['settings']['headerMetro']; ?></a>
                        </span>
                        <span class="header-phone-code font_main">
                            <?= @\Yii::$app->params['settings']['headerPhoneCode']; ?>
                        </span>
                        <span class="header-phone font_main">
                            <?= @\Yii::$app->params['settings']['headerPhone']; ?>
                        </span>
                        <button class="button-css font_main">заказать звонок</button>
                    </div>
                </div>
            </div>
            <div class="header-menu">
                <?= $this->renderFile('@app/views/layouts/_navigation.php'); ?>
            </div>
        </div>
    </div>
    <div class="header-banner font_main">
        <button class="button-css">консультация специалиста</button>
        <?php
        if ($bannerPages = Banners::getBanners('header_banner')) {
            ?>
            <div class="header-carousel">
                <?= Carousel::widget([
                    'items'   => $bannerPages,
                    'options' => [
                        'class'         => 'carousel slide',
                        'data-interval' => @Yii::$app->params['settings']['bannerIntervalOnHeader'] ?: 3000
                    ]
                ]);
                ?>
            </div>
        <?php
        } else {
            ?>
            <div class="header-banner-slider">открой мир заново!</div>
        <?php
        } ?>
        <h1 class="header-banner-text-main">слуховые аппараты</h1>
        <div class="header-banner-text-second">продажа и обслуживание</div>
        <div class="hedge"></div>
    </div>
</header>