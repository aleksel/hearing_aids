<?php
/**
 * @var $item \app\models\Goods
 */
?>
<div class="main-carousel-slide">
    <div class="font_main main-carousel-label">Популярные слуховые аппараты</div>
    <div class="main-carousel-image" style="background: url('<?= $item->image_small[0]->url; ?>') no-repeat 50% 50%"></div>
    <div class="main-carousel-description">
        <div class="font_main main-carousel-title">
            <?= $item->label; ?>
        </div>
        <div class="font_main main-carousel-description-short">
            <?= $item->description_short; ?>
        </div>
        <br/>
        <br/>
        <div class="font_main main-carousel-description-price">
            <?= $item->price; ?> рублей
        </div>
        <br/>
        <div class="font_main main-carousel-description-button">
            <button class="button-css">бесплатная консультация</button>
        </div>
    </div>
</div>
