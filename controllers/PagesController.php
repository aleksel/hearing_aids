<?php

namespace app\controllers;

use Yii;

/**
 * Class PagesController
 * @package app\controllers
 */
class PagesController extends FrontController
{
    public function actionIndex()
    {
        if (!Yii::$app->params['static_page']) {
            throw new \HttpException(Yii::t('app', 'Not found'), 404);
        }

        return $this->render('index', ['page' => Yii::$app->params['static_page']]);
    }
}
