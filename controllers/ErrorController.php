<?php

namespace app\controllers;

use Yii;

class ErrorController extends FrontController
{
    public function actions()
    {
        $this->layout = 'error';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
}
