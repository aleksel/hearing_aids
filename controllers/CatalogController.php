<?php

namespace app\controllers;

use app\models\Goods;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class CatalogController
 * @package app\controllers
 */
class CatalogController extends FrontController
{
    public function actionIndex()
    {
        $goods = Goods::find()
            ->where(['goods.is_showing' => '1'])
            ->joinWith(['files', 'goodsHasFeatures', 'type0'], true)
            ->orderBy(['type' => 'ASC', 'price' => 'ASC'])
            ->all();
        return $this->render('index', ['goods' => $goods]);
    }

    public function actionItem($title)
    {
        $item = Goods::find()
            ->where(['goods.is_showing' => '1', 'goods.url' => $title])
            ->joinWith(['files', 'goodsHasFeatures', 'type0'], true)
            ->one();

        if (!$item) {
            throw new InvalidParamException('Not found', 404);
        }

        $path = Yii::getAlias('@webroot') . Yii::$app->params['goodsFilePath'];

        $withGoods = [];
        if ($item->goodsF) {
            foreach ($item->goodsF as $key => $good) {
                if ($key % 2 !== 0 && $key !== 0) {
                    continue;
                }

                $withGoods[] = [
                    'content' => $this->renderFile(
                        '@app/views/layouts/_with_goods.php',
                        ['item' => $good->goodsS, 'second' => (@$item->goodsF[$key + 1]->goodsS ? : false)]
                    ),
                    'caption' => '',
                    'options' => [''],
                ];
            }
        }

        $similarItem = Goods::find()
            ->where(['goods.type' => $item->type, 'goods.is_showing' => '1'])
            ->joinWith(['files', 'goodsHasFeatures', 'type0'], true)
            ->all();

        $similarGoods = [];
        if ($similarItem) {
            foreach ($similarItem as $key => $good) {
                if ($key % 2 !== 0 && $key !== 0) {
                    continue;
                }

                $similarGoods[] = [
                    'content' => $this->renderFile(
                        '@app/views/layouts/_with_goods.php',
                        ['item' => $good, 'second' => (@$similarItem[$key + 1] ? : false)]
                    ),
                    'caption' => '',
                    'options' => [''],
                ];
            }
        }

        return $this->render(
            'item',
            ['item' => $item, 'path' => $path, 'withGoods' => $withGoods, 'similarGoods' => $similarGoods]
        );
    }
}
