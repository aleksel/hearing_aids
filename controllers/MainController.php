<?php

namespace app\controllers;

use app\models\Goods;
use Yii;

/**
 * Class MainController
 * @package app\controllers
 */
class MainController extends FrontController
{
    public function actionIndex()
    {
        $goods = Goods::find()->where(['is_showing_popular' => '1'])->all();
        $popularGoods = [];
        if ($goods) {
            foreach ($goods as $item) {
                if (@$item->image_small[0]->path) {
                    $popularGoods[] = [
                        'content' => $this->renderFile('@app/views/layouts/_popular_goods.php', ['item' => $item]),
                        'caption' => '',
                        'options' => [''],
                    ];
                }
            }
        }
        return $this->render('index', ['popularGoods' => $popularGoods]);
    }
}
