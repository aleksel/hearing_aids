<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    ''                                                                                      => '@@@@',
    'Are you sure you want to delete this item?'                                            => 'Вы уверены что хотите удалить данный элемент?',
    'Auth Key'                                                                              => 'Авторизационный ключ',
    'Banner Pages'                                                                          => 'Баннеры',
    'Callbacks'                                                                             => '',
    'Change password'                                                                       => 'Изменить пароль',
    'Check your email for further instructions.'                                            => 'Дальнейшие инструкции высланы вам на e-mail.',
    'Create'                                                                                => '<span class="glyphicon glyphicon-plus"></span> Создать',
    'Create {modelClass}'                                                                   => 'Создать {modelClass}',
    'Created At'                                                                            => 'Дата создания',
    'Delete'                                                                                => '<span class="glyphicon glyphicon-trash"></span> Удалить',
    'E-mail'                                                                                => 'E-mail',
    'Edit'                                                                                  => '@@<span class="glyphicon glyphicon-pencil"></span> Редактировать@@',
    'Enter the control panel'                                                               => 'Вход в панель управления',
    'Error'                                                                                 => 'Ошибка',
    'Features'                                                                              => 'Характеристики',
    'Good Types'                                                                            => 'Тип',
    'Goods'                                                                                 => 'Товары',
    'Goods Features Tab'                                                                    => '<small><span class="glyphicon glyphicon-paperclip"></span> Характеристики</small>',
    'Goods Has Features'                                                                    => 'Характеристики',
    'Goods Has Goods'                                                                       => 'Сопутствующие товары',
    'Goods Tab'                                                                             => '<span class="glyphicon glyphicon-asterisk"></span> Товар',
    'ID'                                                                                    => 'ID',
    'If you forgot your password you can '                                                  => 'Если вы забыли пароль, вы можете ',
    'Incorrect username or password.'                                                       => 'Имя пользователя или пароль заданы неверно.',
    'Items count'                                                                           => 'Кол-во записей',
    'List All Goods Features'                                                               => '<span class="glyphicon glyphicon-list"></span> Список всех характеристик',
    'Login'                                                                                 => 'Вход',
    'Menu'                                                                                  => 'Меню',
    'Name'                                                                                  => 'Имя пользователя',
    'New Goods Feature'                                                                     => '<span class="glyphicon glyphicon-plus"></span> Добавить новую характеристику',
    'New Related Good'                                                                      => '<span class="glyphicon glyphicon-plus"></span> Добавить сопутствующий товар',
    'New password was saved.'                                                               => 'Новый пароль был успешно сохранен.',
    'Pages'                                                                                 => 'Страницы',
    'Password'                                                                              => 'Пароль',
    'Password Hash'                                                                         => 'Пароль',
    'Password reset for '                                                                   => 'Сброс пароля для пользователя: ',
    'Password reset token cannot be blank.'                                                 => 'Ключ для сброса пароля не может быть пустым.',
    'Please choose your new password:'                                                      => 'Пожалуйста выбирите новый пароль:',
    'Please contact us if you think this is a server error. Thank you.'                     => 'Пожалуйста свяжитесь с нами если вы считаете что это ошибка сервера. Спасибо.',
    'Please fill out your email. A link to reset password will be sent there.'              => 'Пожалуйста введите свой e-mail. Ссылка для сброса пароля будет выслана вам на почтовый ящик',
    'Related Goods'                                                                         => '<small><span class="glyphicon glyphicon-paperclip"></span> Сопутствующие товары</small>',
    'Remember me'                                                                           => 'Запомнить меня',
    'Request password reset'                                                                => 'Запрос на сброс пароля',
    'Reset'                                                                                 => 'Сброс',
    'Reset Key'                                                                             => 'Ключ сброса пароля',
    'Reset password'                                                                        => 'Сброс пароля',
    'Role'                                                                                  => 'Роль',
    'Save'                                                                                  => '<span class="glyphicon glyphicon-check"></span> Сохранить',
    'Search'                                                                                => 'Поиск',
    'Send'                                                                                  => 'Отправить',
    'Settings'                                                                              => 'Ностройки',
    'Signup'                                                                                => 'Присоединиться',
    'Sorry, we are unable to reset password for email provided.'                            => 'Приносим извинения, но в данный момент мы не можем восстановить ваш пароль.',
    'Status'                                                                                => 'Статус',
    'The above error occurred while the Web server was processing your request.'            => 'Ошибки выше произошли при обработке вашего запроса севером.',
    'There is no user with such email.'                                                     => 'Пользователь с заданым e-mail не найден.',
    'This email address has already been taken.'                                            => 'E-mail уже занят.',
    'This username has already been taken.'                                                 => 'Имя пользователя уже занято.',
    'Type'                                                                                  => '@@Тип@@',
    'Update'                                                                                => '<span class="glyphicon glyphicon-pencil"></span> Редактировать',
    'Update breadcrumbs'                                                                    => 'Редактирование',
    'Update {modelClass}'                                                                   => 'Обновить {modelClass}',
    'Update {modelClass}: '                                                                 => 'Обновить {modelClass}',
    'Updated At'                                                                            => 'Дата обновления',
    'Username'                                                                              => 'Логин',
    'Users'                                                                                 => 'Пользователи',
    'Value'                                                                                 => '@@Значение@@',
    'Wrong password reset token.'                                                           => 'Ключ для сброса пароля задан неверно.',
    'You are not allowed to access this page'                                               => 'У вас не достаточно прав для доступа к этой странице.',
    'feature_id'                                                                            => 'Характеристика',
    'is_showing'                                                                            => 'Отображать',
    'label'                                                                                 => 'Название',
    'reset it'                                                                              => 'его сбросить',
    'value'                                                                                 => 'Значение',
    '{n, plural, =0{Banner Page} =1{Banner Page} =2{Banner Pages} other{Banner Pages}}'     => '{n, plural, =0{Баннеров} =1{Баннер} =2{Баннер} other{Баннеров}}',
    '{n, plural, =0{Callback} =1{Callback} =2{Callbacks} other{Callbacks}}'                 => '',
    '{n, plural, =0{Feature Type} =1{Feature Type} =2{Feature Types} other{Feature Types}}' => '{n, plural, =0{Типов характеристик} =1{Тип Характеристики} =2{Тип характеристики} other{Типы характеристик}}',
    '{n, plural, =0{Feature} =1{Feature} =2{Features} other{Features}}'                     => '{n, plural, =0{Характеристик} =1{Характеристика} =2{Характеристику} other{Характеристик}}',
    '{n, plural, =0{Good Type} =1{Good Type} =2{Good Types} other{Good Types}}'             => '{n, plural, =0{Типов товара} =1{Тип товара} =2{Тип товара} other{Типов товаров}}',
    '{n, plural, =0{Good} =1{Good} =2{Goods} other{Goods}}'                                 => '{n, plural, =0{Товаров} =1{Товар} =2{Товар} other{Товаров}}',
    '{n, plural, =0{Page} =1{Page} =2{Pages} other{Pages}}'                                 => '{n, plural, =0{Страниц} =1{Страница} =2{Страницу} other{Страницы}}',
    '{n, plural, =0{Setting} =1{Setting} =2{Settings} other{Settings}}'                     => '{n, plural, =0{Настроек} =1{Настройка} =2{Настройку} other{Настройки}}',
    '{n, plural, =0{User} =1{User} =2{Users} other{Users}}'                                 => '{n, plural, =0{Пользователей} =1{Пользователь} =2{Пользователя} other{Пользователи}}',
];
